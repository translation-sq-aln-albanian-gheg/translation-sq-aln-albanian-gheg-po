# Translation of GeoLite to Albanian
msgid ""
msgstr ""
"Project-Id-Version: MediaWiki 1.17alpha (r66640)\n"
"Report-Msgid-Bugs-To: http://translatewiki.net\n"
"POT-Creation-Date: 2010-05-19 18:09:23+0000\n"
"Language-Team: Albanian\n"
"Content-Type: text-plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: MediaWiki Translate extension 2010-05-15\n"
"X-Language-Code: sq\n"
"X-Message-Group: ext-geolite\n"

#. '''GeoIP''' is a registered trademarks of MaxMind, Inc. http://www.maxmind.com/<br />Do not translate it.
#. 
#. {{desc}}
msgctxt "geolite-desc"
msgid "Lightweight GeoIp Redirection"
msgstr ""

