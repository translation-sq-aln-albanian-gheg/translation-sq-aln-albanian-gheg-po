# Translation of QPoll to Albanian
msgid ""
msgstr ""
"Project-Id-Version: MediaWiki 1.17alpha (r66640)\n"
"Report-Msgid-Bugs-To: http://translatewiki.net\n"
"POT-Creation-Date: 2010-05-19 18:20:26+0000\n"
"Language-Team: Albanian\n"
"Content-Type: text-plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: MediaWiki Translate extension 2010-05-15\n"
"X-Language-Code: sq\n"
"X-Message-Group: ext-qpoll\n"

#. Special page name in [[Special:SpecialPages]]
msgctxt "pollresults"
msgid "Results of the polls on this site"
msgstr ""

#. {{desc}} Important notice: Categories can be grouped into category groups, which are internally referred as "spans". Such grouped categories become "subcategories". While the extension evolved, these groups were consequentially called as "spans", "metacategories", "category groups". Please read the on-line documentation carefully before translating.
msgctxt "qp_desc"
msgid "Allows creation of polls"
msgstr ""

#:
msgctxt "qp_desc-sp"
msgid "[[Special:PollResults|Special page]] for viewing results of the polls"
msgstr ""

#:
msgctxt "qp_result_NA"
msgid "Not answered"
msgstr ""

#. {{Identical|Syntax error}}
msgctxt "qp_result_error"
msgid "Syntax error"
msgstr ""

#. {{Identical|Vote}}
msgctxt "qp_vote_button"
msgid "Vote"
msgstr ""

#:
msgctxt "qp_vote_again_button"
msgid "Change your vote"
msgstr ""

#:
msgctxt "qp_polls_list"
msgid "List all polls"
msgstr ""

#:
msgctxt "qp_users_list"
msgid "List all users"
msgstr ""

#:
msgctxt "qp_browse_to_poll"
msgid "Browse to $1"
msgstr ""

#:
msgctxt "qp_browse_to_user"
msgid "Browse to $1"
msgstr ""

#:
msgctxt "qp_votes_count"
msgid "$1 {{PLURAL:$1|vote|votes}}"
msgstr ""

#. "Source" is the link text for a link to the page where the poll is defined.
#. {{Identical|Source}}
msgctxt "qp_source_link"
msgid "Source"
msgstr ""

#. {{Identical|Statistics}}
msgctxt "qp_stats_link"
msgid "Statistics"
msgstr ""

#. {{Identical|User}}
msgctxt "qp_users_link"
msgid "Users"
msgstr ""

#:
msgctxt "qp_voice_link"
msgid "User voice"
msgstr ""

#. At the moment of query generation there was no user answer for the selected poll yet. Question mark encourages wiki administrator to re-submit the query again.
msgctxt "qp_voice_link_inv"
msgid "User voice?"
msgstr ""

#. Parameters:
#. * $1 is the number of polls participated in.
#. * $2 is the name of the user this message refers to (optional - use for GENDER)
msgctxt "qp_user_polls_link"
msgid "Participated in $1 {{PLURAL:$1|poll|polls}}"
msgstr ""

#. Parameters:
#. * $1 is the name of the user this message refers to (optional - use for GENDER)
msgctxt "qp_user_missing_polls_link"
msgid "No participation"
msgstr ""

#:
msgctxt "qp_not_participated_link"
msgid "Not participated"
msgstr ""

#:
msgctxt "qp_order_by_username"
msgid "Order by username"
msgstr ""

#:
msgctxt "qp_order_by_polls_count"
msgid "Order by polls count"
msgstr ""

#. Parameters:
#. * $1 is a link to the page page name the poll is on with the page title as link label
#. * $2 is the poll name in plain text
#. * $3 is a link to the poll statistics with link label {{msg-mw|qp_stats_link}}
msgctxt "qp_results_line_qupl"
msgid "Page \"$1\" Poll \"$2\": $3"
msgstr ""

#. Parameters:
#. * $1 is a link to the page page name the poll is on with the page title as link label
#. * $2 is the poll name in plain text
#. * $3 is a link to the poll with link label {{msg-mw|qp_source_link}}
#. * $4 is a link to the poll statistics with link label {{msg-mw|qp_stats_link}}
#. * $5 is a link to the users that participated in the poll with link label {{msg-mw|qp_users_link}}
#. * $6 is a link to the with link label {{msg-mw|qp_not_participated_link}}
msgctxt "qp_results_line_qpl"
msgid "Page \"$1\" Poll \"$2\": $3, $4, $5, $6"
msgstr ""

#:
msgctxt "qp_header_line_qpul"
msgid "$1 [ Page \"$2\" Poll \"$3\" ]"
msgstr ""

#:
msgctxt "qp_export_to_xls"
msgid "Export statistics into XLS format"
msgstr ""

#:
msgctxt "qp_users_answered_questions"
msgid "$1 {{PLURAL:$1|user|users}} answered to the questions"
msgstr ""

#:
msgctxt "qp_func_no_such_poll"
msgid "No such poll ($1)"
msgstr ""

#:
msgctxt "qp_func_missing_question_id"
msgid "Please specify an existing question id (starting from 1) for the poll $1"
msgstr ""

#:
msgctxt "qp_func_invalid_question_id"
msgid "Invalid question id=$2 (not a number) for the poll $1"
msgstr ""

#:
msgctxt "qp_func_missing_proposal_id"
msgid "Please specify an existing proposal id (starting from 0) for the poll $1, question $2"
msgstr ""

#:
msgctxt "qp_func_invalid_proposal_id"
msgid "Invalid proposal id=$3 (not a number) for the poll $1, question $2"
msgstr ""

#:
msgctxt "qp_error_no_such_poll"
msgid "No such poll ($1).\nMake sure that the poll declared and saved, also be sure to use address delimiter character #"
msgstr ""

#:
msgctxt "qp_error_in_question_header"
msgid "Invalid question header: $1"
msgstr ""

#:
msgctxt "qp_error_id_in_stats_mode"
msgid "Cannot declare an ID of the poll in statistical mode"
msgstr ""

#:
msgctxt "qp_error_dependance_in_stats_mode"
msgid "Cannot declare dependance chain of the poll in statistical mode"
msgstr ""

#:
msgctxt "qp_error_no_stats"
msgid "No statistical data is available, because no one has voted for this poll, yet (address=$1)"
msgstr ""

#:
msgctxt "qp_error_address_in_decl_mode"
msgid "Cannot get an address of the poll in declaration mode"
msgstr ""

#:
msgctxt "qp_error_question_not_implemented"
msgid "Questions of such type are not implemented: $1"
msgstr ""

#:
msgctxt "qp_error_invalid_question_type"
msgid "Invalid question type: $1"
msgstr ""

#:
msgctxt "qp_error_type_in_stats_mode"
msgid "Question type cannot be defined in statistical display mode: $1"
msgstr ""

#:
msgctxt "qp_error_no_poll_id"
msgid "Poll tag has no id attribute defined."
msgstr ""

#:
msgctxt "qp_error_invalid_poll_id"
msgid "Invalid poll id (id=$1).\nPoll id may contain only letters, numbers and space character"
msgstr ""

#:
msgctxt "qp_error_already_used_poll_id"
msgid "The poll id has already been used on this page (id=$1)."
msgstr ""

#:
msgctxt "qp_error_invalid_dependance_value"
msgid "The poll (id=$1) dependance chain has invalid value of dependance attribute (dependance=\"$2\")"
msgstr ""

#:
msgctxt "qp_error_missed_dependance_title"
msgid "The poll (id=$1) is dependant on the another poll (id=$3) from page [[$2]], but the title [[$2]] was not found.\nEither remove the dependance attribute, or restore [[$2]]"
msgstr ""

#. Parameters:
#. {| cellpadding="0" cellspacing="0" border="0"
#. !$1||&nbsp;
#. | is the poll ID of the poll having an error.
#. |-
#. !$2||
#. |is a link to the page with the poll, that this erroneous poll depends on.
#. |-
#. !$3||
#. |is the poll ID of the poll, which this erroneous poll depends on.
#. |}
msgctxt "qp_error_missed_dependance_poll"
msgid "The poll (id=$1) is dependant on the another poll (id=$3) at page $2, but that poll does not exists or has not been saved yet.\nEither remove the dependance attribute, or create the poll with id=$3 at the page $2 and save it.\nTo save a poll, submit it while not answering to any proposal questions."
msgstr ""

#:
msgctxt "qp_error_vote_dependance_poll"
msgid "Please vote for poll $1 first."
msgstr ""

#. There cannot be more category groups defined than the total count of subcategories.
msgctxt "qp_error_too_many_spans"
msgid "Too many category groups for the total number of subcategories defined"
msgstr ""

#:
msgctxt "qp_error_unanswered_span"
msgid "Unanswered subcategory"
msgstr ""

#:
msgctxt "qp_error_non_unique_choice"
msgid "This question requires unique proposal answer"
msgstr ""

#:
msgctxt "qp_error_category_name_empty"
msgid "Category name is empty"
msgstr ""

#:
msgctxt "qp_error_proposal_text_empty"
msgid "Proposal text is empty"
msgstr ""

#:
msgctxt "qp_error_too_few_categories"
msgid "At least two categories must be defined"
msgstr ""

#. Every category group should include at least two subcategories
msgctxt "qp_error_too_few_spans"
msgid "Every category group must contain at least two subcategories"
msgstr ""

#:
msgctxt "qp_error_no_answer"
msgid "Unanswered proposal"
msgstr ""

#:
msgctxt "qp_error_unique"
msgid "Question of type unique() has more proposals than possible answers defined: impossible to complete"
msgstr ""

