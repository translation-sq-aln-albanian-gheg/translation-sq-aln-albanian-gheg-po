# Translation of Right Functions to Gheg Albanian
msgid ""
msgstr ""
"Project-Id-Version: MediaWiki 1.17alpha (r66640)\n"
"Report-Msgid-Bugs-To: http://translatewiki.net\n"
"POT-Creation-Date: 2010-05-19 17:31:26+0000\n"
"Language-Team: Gheg Albanian\n"
"Content-Type: text-plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: MediaWiki Translate extension 2010-05-15\n"
"X-Language-Code: aln\n"
"X-Message-Group: ext-rightfunctions\n"

#. Short description of this extension, shown in [[Special:Version]]. Do not translate or change links.
msgctxt "rightfunctions-desc"
msgid "Permission-based parser functions"
msgstr ""

#:
msgctxt "rightfunctions-casc"
msgid "(from cascading sources)"
msgstr ""

#:
msgctxt "rightfunctions-local"
msgid "(from local page)"
msgstr ""

#:
msgctxt "rightfunctions-ns"
msgid "(from namespace protection)"
msgstr ""

