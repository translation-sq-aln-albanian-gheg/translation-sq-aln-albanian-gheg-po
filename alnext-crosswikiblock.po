# Translation of Crosswiki Block to Gheg Albanian
msgid ""
msgstr ""
"Project-Id-Version: MediaWiki 1.17alpha (r66640)\n"
"Report-Msgid-Bugs-To: http://translatewiki.net\n"
"POT-Creation-Date: 2010-05-19 17:13:03+0000\n"
"Language-Team: Gheg Albanian\n"
"Content-Type: text-plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: MediaWiki Translate extension 2010-05-15\n"
"X-Language-Code: aln\n"
"X-Message-Group: ext-crosswikiblock\n"

#. Extension description displayed on [[Special:Version]].
msgctxt "crosswikiblock-desc"
msgid "Allows to block users on other wikis using a [[Special:Crosswikiblock|special page]]"
msgstr ""

#:
msgctxt "crosswikiblock"
msgid "Block user on other wiki"
msgstr ""

#:
msgctxt "crosswikiblock-header"
msgid "This page allows to block user on other wiki.\nPlease check if you are allowed to act on this wiki and your actions match all policies."
msgstr ""

#:
msgctxt "crosswikiblock-target"
msgid "IP address or username and destination wiki:"
msgstr ""

#. {{Identical|Expiry}}
msgctxt "crosswikiblock-expiry"
msgid "Expiry:"
msgstr ""

#. {{Identical|Reason}}
msgctxt "crosswikiblock-reason"
msgid "Reason:"
msgstr ""

#. {{Identical|Block this user}}
msgctxt "crosswikiblock-submit"
msgid "Block this user"
msgstr ""

#. {{Identical|Block anonymous users only}}
msgctxt "crosswikiblock-anononly"
msgid "Block anonymous users only"
msgstr ""

#. {{Identical|Prevent account creation}}
msgctxt "crosswikiblock-nocreate"
msgid "Prevent account creation"
msgstr ""

#. {{Identical|Automatically block ...}}
msgctxt "crosswikiblock-autoblock"
msgid "Automatically block the last IP address used by this user, and any subsequent IP addresses they try to edit from"
msgstr ""

#. {{Identical|Prevent user from sending e-mail}}
msgctxt "crosswikiblock-noemail"
msgid "Prevent user from sending e-mail"
msgstr ""

#:
msgctxt "crosswikiunblock"
msgid "Unblock user on other wiki"
msgstr ""

#:
msgctxt "crosswikiunblock-header"
msgid "This page allows to unblock user on other wiki.\nPlease check if you are allowed to act on this wiki and your actions match all policies."
msgstr ""

#:
msgctxt "crosswikiunblock-user"
msgid "Username, IP address or block ID and destination wiki:"
msgstr ""

#. {{Identical|Reason}}
msgctxt "crosswikiunblock-reason"
msgid "Reason:"
msgstr ""

#:
msgctxt "crosswikiunblock-submit"
msgid "Unblock this user"
msgstr ""

#:
msgctxt "crosswikiunblock-success"
msgid "User '''$1''' unblocked successfully.\n\nReturn to:\n* [[Special:CrosswikiBlock|Block form]]\n* [[$2]]"
msgstr ""

#:
msgctxt "crosswikiblock-nousername"
msgid "No username was given"
msgstr ""

#:
msgctxt "crosswikiblock-local"
msgid "Local blocks are not supported via this interface. Use [[Special:BlockIP|{{int:blockip}}]]"
msgstr ""

#:
msgctxt "crosswikiblock-dbnotfound"
msgid "Database $1 does not exist"
msgstr ""

#:
msgctxt "crosswikiblock-noname"
msgid "\"$1\" is not a valid username."
msgstr ""

#:
msgctxt "crosswikiblock-nouser"
msgid "User \"$3\" is not found."
msgstr ""

#:
msgctxt "crosswikiblock-noexpiry"
msgid "Invalid expiry: $1."
msgstr ""

#:
msgctxt "crosswikiblock-noreason"
msgid "No reason specified."
msgstr ""

#. {{Identical|Invalid edit token}}
msgctxt "crosswikiblock-notoken"
msgid "Invalid edit token."
msgstr ""

#. {{Identical|$1 is already blocked}}
msgctxt "crosswikiblock-alreadyblocked"
msgid "User $3 is already blocked."
msgstr ""

#:
msgctxt "crosswikiblock-noblock"
msgid "This user is not blocked."
msgstr ""

#:
msgctxt "crosswikiblock-success"
msgid "User '''$3''' blocked successfully.\n\nReturn to:\n* [[Special:CrosswikiBlock|Block form]]\n* [[$4]]"
msgstr ""

#:
msgctxt "crosswikiunblock-local"
msgid "Local unblocks are not supported via this interface. Use [[Special:IPBlockList|{{int:ipblocklist}}]]"
msgstr ""

#. {{doc-right|crosswikiblock}}
msgctxt "right-crosswikiblock"
msgid "Block and unblock users on other wikis"
msgstr ""

