# translate wiki getter
# downloads the translatewiki po files for albanian standard sq and gheg dialect aln
#  This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.

#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.

#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use warnings;
my @projects = qw[
core-0-mostused
core-1.15
core-1.16
ext-0-all
ext-0-wikihow
ext-0-wikimedia
ext-0-wikitravel
ext-abc
ext-absenteelandlord
ext-abusefilter
ext-adminlinks
ext-advancedrandom
ext-ajaxlogin
ext-ajaxquerypages
ext-ajaxshoweditors
ext-amazonplus
ext-antibot
ext-antispoof
ext-apc
ext-apisvgproxy
ext-asksql
ext-assertedit
ext-authorprotect
ext-autoincrement
ext-babel
ext-backandforth
ext-badimage
ext-blahtex
ext-blocktitles
ext-bookinformation
ext-breadcrumbs
ext-call
ext-categoryfeed
ext-categoryintersection
ext-categoryonupload
ext-categorystepper
ext-categorytests
ext-categorytree
ext-categorywatch
ext-centralauth
ext-centralnotice
ext-changeauthor
ext-charinsert
ext-checkpoint
ext-checkuser
ext-chemistry
ext-cite
ext-citespecial
ext-cldr
ext-cleanchanges
ext-clientside
ext-closewikis
ext-codereview
ext-collection-0-all
ext-collection-core
ext-collection-other
ext-commentbox
ext-commentpages
ext-commentspammer
ext-communityvoice
ext-conditionalshowsection
ext-configure
ext-configuresettings
ext-confirmaccount
ext-confirmedit
ext-confirmeditfancycaptcha
ext-confirmeditquestycaptcha
ext-contactpage
ext-contributionreporting
ext-contributionscores
ext-contributionseditcount
ext-contributiontracking
ext-contributors
ext-contributorsaddon
ext-cooperationstatistics
ext-countedits
ext-createbox
ext-crossnamespacelinks
ext-crosswikiblock
ext-crowdauthentication
ext-css
ext-datatransfer
ext-delayeddefinition
ext-deletebatch
ext-deletequeue
ext-di-pfpg
ext-didyoumean
ext-discussionthreading
ext-dismissablesitenotice
ext-doublewiki
ext-dplforum
ext-drafts
ext-duplicator
ext-dynamicsidebar
ext-editcount
ext-editmessages
ext-editown
ext-editsectionhilitelink
ext-editsimilar
ext-editsubpages
ext-edituser
ext-emailaddressimage
ext-emergencydesysop
ext-errorhandler
ext-eval
ext-expandtemplates
ext-extensiondistributor
ext-externaldata
ext-externalpages
ext-farmer
ext-fckeditor
ext-filepagemasking
ext-findspam
ext-firefoggchunkedupload
ext-fixedimage
ext-flaggedrevs-0-all
ext-flaggedrevs-configuredpages
ext-flaggedrevs-flaggedrevs
ext-flaggedrevs-oldreviewedpages
ext-flaggedrevs-problemchanges
ext-flaggedrevs-qualityoversight
ext-flaggedrevs-reviewedpages
ext-flaggedrevs-reviewedversions
ext-flaggedrevs-stabilization
ext-flaggedrevs-stablepages
ext-flaggedrevs-unreviewedpages
ext-flaggedrevs-validationstatistics
ext-flvhandler
ext-forcepreview
ext-form
ext-formatemail
ext-framedvideo
ext-fundraiserportal
ext-gadgets
ext-geolite
ext-getfamily
ext-globalblocking
ext-globalusage
ext-globaluserrights
ext-gnuplot
ext-googleadsense
ext-googleanalytics
ext-googlemaps
ext-googlenewssitemap
ext-gotocategory
ext-grouppermissionsmanager
ext-groupssidebar
ext-hidenamespace
ext-honeypotintegration
ext-htmlets
ext-i18ntags
ext-icon
ext-imagemap
ext-imagetagging
ext-importfreeimages
ext-importusers
ext-imstatus
ext-indexfunction
ext-inputbox
ext-inspectcache
ext-interlanguage
ext-intersection
ext-interwiki
ext-interwikilist
ext-invitations
ext-ipauth
ext-js2support
ext-jskit
ext-labeledsectiontransclusion
ext-languageselector
ext-lastuserlogin
ext-latexdoc
ext-ldapauthentication
ext-linkopenid
ext-liquidthreads
ext-localisationupdate
ext-lockdown
ext-logentry
ext-lookupuser
ext-lua
ext-magicnonumberedheadings
ext-maintenance
ext-maps
ext-masseditregex
ext-mathstatfunctions
ext-mediafunctions
ext-metadataedit
ext-metakeywords
ext-metavidwiki
ext-microid
ext-minidonation
ext-minify
ext-minimumnamelength
ext-minipreview
ext-mostrevisors
ext-multiboilerplate
ext-multiupload
ext-mwreleases
ext-mwsearch
ext-naturallanguagelist
ext-navigationpopups
ext-negref
ext-networkauth
ext-newestpages
ext-news
ext-newschannel
ext-newusermessage
ext-newusernotification
ext-nsfilerepo
ext-nssmysqlauth
ext-nuke
ext-oai
ext-ogghandler
ext-onlinestatus
ext-openid
ext-opensearchxml
ext-othersites
ext-oversight
ext-packageforce
ext-pageby
ext-pagedtiffhandler
ext-parserfunctions
ext-parserwiki
ext-passwordreset
ext-patroller
ext-pdfhandler
ext-peoplecategories
ext-pipeescape
ext-piwik
ext-player
ext-plotters
ext-pnghandler
ext-poem
ext-poolcounter
ext-postcomment
ext-povwatch
ext-preloader
ext-profilemonitor
ext-proofreadpage
ext-protectsection
ext-psinotocnum
ext-purge
ext-purgecache
ext-qpoll
ext-quiz
ext-randomimage
ext-randomincategory
ext-randomrootpage
ext-randomuserswithavatars
ext-readerfeedback-0-all
ext-readerfeedback-ratedpages
ext-readerfeedback-ratinghistory
ext-readerfeedback-readerfeedback
ext-recordadmin
ext-redirect
ext-refhelper
ext-refreshspecial
ext-regexblock
ext-regexfunctions
ext-renameuser
ext-replaceset
ext-replacetext
ext-rightfunctions
ext-rped
ext-rt
ext-scanset
ext-searchbox
ext-securepasswords
ext-securepoll
ext-seealso
ext-selectcategory
ext-semanticcompoundqueries
ext-semanticdrilldown
ext-semanticforms
ext-semanticgallery
ext-semanticinternalobjects
ext-semanticmaps
ext-semanticmediawiki
ext-semanticresultformats
ext-semantictasks
ext-sendmailtowiki
ext-shareduserrights
ext-showprocesslist
ext-signdocument
ext-signdocumentspecial
ext-signdocumentspecialcreate
ext-simpleantispam
ext-simplesecurity
ext-sitematrix
ext-skinperpage
ext-smoothgallery
ext-socialprofile-0-all
ext-socialprofile-systemgifts
ext-socialprofile-useractivity
ext-socialprofile-userboard
ext-socialprofile-usergifts
ext-socialprofile-userprofile
ext-socialprofile-userrelationship
ext-socialprofile-userstats
ext-socialprofile-userwelcome
ext-spamblacklist
ext-spamdifftool
ext-spamregex
ext-specialfilelist
ext-sphinxsearch
ext-stalepages
ext-stalkerlog
ext-stockcharts
ext-storyboard
ext-strategywiki-activetaskforces
ext-stringfunctions
ext-subpagelist3
ext-svgzoom
ext-syntaxhighlightgeshi
ext-tab0
ext-tablemod
ext-talkhere
ext-tasks
ext-templateinfo
ext-templatelink
ext-tidytab
ext-timeline
ext-titleblacklist
ext-titlekey
ext-todo
ext-todotasks
ext-tooltip
ext-torblock
ext-translate-0-all
ext-translate-core
ext-translate-pagetranslation
ext-transliterator
ext-trustedxff
ext-tspoll
ext-ui-0-all
ext-ui-clicktracking
ext-ui-optin
ext-ui-optinlink
ext-ui-prefstats
ext-ui-prefswitch
ext-ui-prefswitchlink
ext-ui-updatemyprefs
ext-ui-usabilityinitiative
ext-ui-userdailycontribs
ext-ui-vector
ext-ui-vector-collapsiblenav
ext-ui-vector-editwarning
ext-ui-vector-simplesearch
ext-ui-wikieditor
ext-ui-wikieditor-addmediawizard
ext-ui-wikieditor-highlight
ext-ui-wikieditor-preview
ext-ui-wikieditor-publish
ext-ui-wikieditor-templateeditor
ext-ui-wikieditor-templates
ext-ui-wikieditor-toc
ext-ui-wikieditor-toolbar
ext-ukgeocodingformaps
ext-unicodeconverter
ext-uniwiki-0-all
ext-uniwiki-authors
ext-uniwiki-autocreatecategorypages
ext-uniwiki-catboxattop
ext-uniwiki-createpage
ext-uniwiki-csshooks
ext-uniwiki-customtoolbar
ext-uniwiki-formatchanges
ext-uniwiki-formatsearch
ext-uniwiki-genericeditpage
ext-uniwiki-javascript
ext-uniwiki-layouts
ext-uniwiki-mootools12core
ext-uploadblacklist
ext-usagestatistics
ext-usercontactlinks
ext-userimages
ext-usermerge
ext-useroptionstats
ext-userpageeditprotection
ext-userrightsnotif
ext-userthrottle
ext-validator
ext-vote
ext-watchers
ext-watchsubpages
ext-webchat
ext-webstore
ext-whatismyip
ext-whitelistedit
ext-whoiswatching
ext-whosonline
ext-widgets
ext-wikiarticlefeeds
ext-wikiathome
ext-wikidata
ext-wikihiero
ext-wikilog
ext-wikilogfeed
ext-wikimediaincubator
ext-wikimedialicensetexts
ext-wikimediamessages
ext-wikitextloggedinout
ext-woopra
ext-youtubeauthsub
ext-yui
out-freecol
out-fudforum
out-mantis
out-mantis-mantiscoreformatting
out-mantis-mantisgraph
out-mantis-xmlimportexport
out-mwlibrl
out-nocc
out-okawix-doc
out-okawix-dtd
out-okawix-prop
out-openlayers
out-osm-potlatch
out-osm-site
out-statusnet
out-voctrain
out-wikiblame
out-wikimediamobile
out-wikireader
page|Osm:Potlatch-help-html
page|Project list
page|Translating:How to start
page|Translating:Intro
page|User:FuzzyBot
wiki-betawiki
wikia-0-all
wikia-actionpanel
wikia-ajaxpoll
wikia-autocreatewiki
wikia-badges
wikia-blogs
wikia-categoryhubs
wikia-categoryselect
wikia-contesttool
wikia-createpage
wikia-daemonloader
wikia-editaccount
wikia-editingtips
wikia-editresearch
wikia-forcesubjectfornewsection
wikia-founderemails
wikia-globalwatchlist
wikia-graceexpired
wikia-hawelcome
wikia-helperpanel
wikia-imageplaceholder
wikia-interstitial-outboundscreen
wikia-interwikidispatcher
wikia-linksuggest
wikia-lookupcontribs
wikia-magcloud
wikia-mostpopulararticles
wikia-mostpopularcategories
wikia-mostvisitedpages
wikia-multitasks
wikia-myhome
wikia-neweditpage
wikia-our404handler
wikia-problemreports
wikia-quickcreate
wikia-search
wikia-searchranktracker
wikia-sendtoafriend
wikia-sharefeature
wikia-sitewidemessages
wikia-specialinterwikiedit
wikia-specialminiupload
wikia-specialmultiplelookup
wikia-specialprivatedomains
wikia-specialprotectsite
wikia-specialwidgetdashboard
wikia-staffpowers
wikia-tagsreport
wikia-taskmanager
wikia-textregex
wikia-theorytab
wikia-titleedit
wikia-videoembedtool
wikia-whereisextension
wikia-widgetspecialpage
wikia-wikiaircgateway
wikia-wikiaminiupload
wikia-wikiaphotogallery
wikia-wikiastats
wikia-wikiaupload
wikia-wikiavideo
wikia-wikiavideoadd
wikia-wikiawidget
wikia-wikifactory
wikia-wikifactorydumps
wikia-wikifactoryreporter
];
   use WWW::Mechanize;
    my $mech = WWW::Mechanize->new();



foreach my $l (qw[aln sq ])
{
    foreach my $p (@projects)
    {
	print $p;
	my $url= "http://translatewiki.net/w/i.php?title=Special%3ATranslate&task=export-as-po&group=${p}&language=${l}&limit=2500";
	#$mech->get( $url );
	my $tempfile  = "$l$p.po.gz";
	print "to get $url $tempfile\n";
	$mech->get( $url, ':content_file' => $tempfile );
	system ("gunzip -f $l$p.po.gz");
	system ("git commit -a -m \"update\"");
	system ("git push origin master");
    }
}


