# Translation of Community Voice to Gheg Albanian
msgid ""
msgstr ""
"Project-Id-Version: MediaWiki 1.17alpha (r66640)\n"
"Report-Msgid-Bugs-To: http://translatewiki.net\n"
"POT-Creation-Date: 2010-05-19 17:11:09+0000\n"
"Language-Team: Gheg Albanian\n"
"Content-Type: text-plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: MediaWiki Translate extension 2010-05-15\n"
"X-Language-Code: aln\n"
"X-Message-Group: ext-communityvoice\n"

#:
msgctxt "communityvoice"
msgid "Community Voice"
msgstr ""

#. Description for the [http://www.mediawiki.org/wiki/Extension:CommunityVoice Community Voice extension]
msgctxt "communityvoice-desc"
msgid "Community participation tools"
msgstr ""

#:
msgctxt "communityvoice-ratings"
msgid "Ratings"
msgstr ""

#:
msgctxt "communityvoice-ratings-scale-status-sending"
msgid "Sending..."
msgstr ""

#:
msgctxt "communityvoice-ratings-scale-status-error"
msgid "Error sending!"
msgstr ""

#:
msgctxt "communityvoice-ratings-scale-status-thanks"
msgid "Thanks for voting!"
msgstr ""

#. Guessing:
#. 
#. * $1 / 5: rating
#. * $2: number of votes
msgctxt "communityvoice-ratings-scale-stats"
msgid "$1 / 5 ($2 {{PLURAL:$2|vote|votes}} cast)"
msgstr ""

#:
msgctxt "communityvoice-ratings-error-no-category"
msgid "Category attribute missing in rating tag."
msgstr ""

#:
msgctxt "communityvoice-ratings-error-no-title"
msgid "Title attribute missing in rating tag."
msgstr ""

