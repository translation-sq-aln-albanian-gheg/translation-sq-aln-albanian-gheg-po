# Translation of Send To A Friend to Gheg Albanian
msgid ""
msgstr ""
"Project-Id-Version: MediaWiki 1.17alpha (r66640)\n"
"Report-Msgid-Bugs-To: http://translatewiki.net\n"
"POT-Creation-Date: 2010-05-19 17:52:36+0000\n"
"Language-Team: Gheg Albanian\n"
"Content-Type: text-plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: MediaWiki Translate extension 2010-05-15\n"
"X-Language-Code: aln\n"
"X-Message-Group: wikia-sendtoafriend\n"

#:
msgctxt "invitespecialpage"
msgid "Invite friends to join Wikia"
msgstr ""

#:
msgctxt "sendtoafriend-desc"
msgid "[[Special:InviteSpecialPage|Invite friends to join Wikia]]"
msgstr ""

#:
msgctxt "sendtoafriend-button-desc"
msgid "Displays a \"Send to a friend\" button in pages"
msgstr ""

#:
msgctxt "stf_button"
msgid "Send this article to a friend"
msgstr ""

#:
msgctxt "stf_after_reg"
msgid "[[Special:InviteSpecialPage|Invite a friend to join Wikia!]]"
msgstr ""

#:
msgctxt "stf_subject"
msgid "$2 has sent you an article from $1!"
msgstr ""

#:
msgctxt "stf_confirm"
msgid "Message sent! Invite others?"
msgstr ""

#:
msgctxt "stf_error"
msgid "Error sending e-mail."
msgstr ""

#:
msgctxt "stf_error_name"
msgid "You didn't provide your name."
msgstr ""

#:
msgctxt "stf_error_from"
msgid "You didn't provide your e-mail address."
msgstr ""

#:
msgctxt "stf_error_to"
msgid "You didn't provide your friend's e-mail address."
msgstr ""

#:
msgctxt "stf_frm1"
msgid "Your e-mail address:"
msgstr ""

#:
msgctxt "stf_frm2"
msgid "E-mail addresses (More than one? Separate with commas)"
msgstr ""

#:
msgctxt "stf_msg_label"
msgid "Message to be sent"
msgstr ""

#. {{Identical|Name}}
msgctxt "stf_name_label"
msgid "Your name"
msgstr ""

#:
msgctxt "stf_email_label"
msgid "Your e-mail address"
msgstr ""

#:
msgctxt "stf_frm3_send"
msgid "Hi!\n\n$1 thought you'd like this page from Wikia!\n\n$2\n\nCome check it out!"
msgstr ""

#:
msgctxt "stf_frm3_invite"
msgid "Hi!\n\nI just joined this wiki at Wikia...  $1\n\nCome check it out!"
msgstr ""

#:
msgctxt "stf_frm4_send"
msgid "Send"
msgstr ""

#. {{Identical|Cancel}}
msgctxt "stf_frm4_cancel"
msgid "Cancel"
msgstr ""

#. Button text
msgctxt "stf_frm4_invite"
msgid "Send invite!"
msgstr ""

#:
msgctxt "stf_multiemail"
msgid "Send to more than one recipient?"
msgstr ""

#:
msgctxt "stf_frm5"
msgid "(the URL of this site will be appended to your message)"
msgstr ""

#:
msgctxt "stf_frm6"
msgid "Close this window"
msgstr ""

#. Needs plural support
msgctxt "stf_throttle"
msgid "For security reasons, you can only send $1 {{PLURAL:$1|invite|invites}} a day."
msgstr ""

#:
msgctxt "stf_abuse"
msgid "This e-mail was sent by $1 via Wikia.\nIf you think this was sent in error, please let us know at support@wikia.com."
msgstr ""

#. Supports plural for $1.
msgctxt "stf_ctx_invite"
msgid "More than one? Separate with commas - up to $1!"
msgstr ""

#:
msgctxt "stf_ctx_check"
msgid "Check contacts"
msgstr ""

#:
msgctxt "stf_ctx_empty"
msgid "You have no contacts in this account."
msgstr ""

#:
msgctxt "stf_ctx_invalid"
msgid "Login or password you typed is invalid. Please try again."
msgstr ""

#:
msgctxt "stf_sending"
msgid "Please wait..."
msgstr ""

#:
msgctxt "stf_email_sent"
msgid "Send confirmation"
msgstr ""

#:
msgctxt "stf_back_to_article"
msgid "Back to article"
msgstr ""

#:
msgctxt "stf_most_emailed"
msgid "Most e-mailed articles on $1 today:"
msgstr ""

#:
msgctxt "stf_most_popular"
msgid "Most popular articles on $1:"
msgstr ""

#:
msgctxt "stf_choose_from_existing"
msgid "Choose from your existing contacts:"
msgstr ""

#:
msgctxt "stf_add_emails"
msgid "Add e-mail addresses:"
msgstr ""

#:
msgctxt "stf_your_email"
msgid "Your e-mail service"
msgstr ""

#:
msgctxt "stf_your_login"
msgid "Your login name"
msgstr ""

#:
msgctxt "stf_your_password"
msgid "Your password"
msgstr ""

#. {{Identical|Name}}
msgctxt "stf_your_name"
msgid "Your name"
msgstr ""

#:
msgctxt "stf_your_address"
msgid "Your e-mail address"
msgstr ""

#. Apparently the pipe character is replaced with an HTML break. Very strange coding practice. The whole message is a column header, I think.
msgctxt "stf_your_friends"
msgid "Your friend's|e-mail addresses"
msgstr ""

#:
msgctxt "stf_we_dont_keep"
msgid "We do not keep this e-mail address and password"
msgstr ""

#:
msgctxt "stf_need_approval"
msgid "No e-mails are sent without your approval"
msgstr ""

#. {{Identical|Message}}
msgctxt "stf_message"
msgid "Message"
msgstr ""

#:
msgctxt "stf_instructions"
msgid "1. Select friends.|2. Click \"$1\""
msgstr ""

#:
msgctxt "stf_select_all"
msgid "Select all"
msgstr ""

#:
msgctxt "stf_select_friends"
msgid "Select friends:"
msgstr ""

