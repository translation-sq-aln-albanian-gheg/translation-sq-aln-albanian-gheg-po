# Translation of Category Watch to Albanian
msgid ""
msgstr ""
"Project-Id-Version: MediaWiki 1.17alpha (r66640)\n"
"Report-Msgid-Bugs-To: http://translatewiki.net\n"
"POT-Creation-Date: 2010-05-19 17:59:34+0000\n"
"Language-Team: Albanian\n"
"Content-Type: text-plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: MediaWiki Translate extension 2010-05-15\n"
"X-Language-Code: sq\n"
"X-Message-Group: ext-categorywatch\n"

#. Short description of this extension, shown on [[Special:Version]]. Do not translate or change links.
msgctxt "categorywatch-desc"
msgid "Extends watchlist functionality to include notification about membership changes of watched categories"
msgstr ""

#:
msgctxt "categorywatch-emailsubject"
msgid "Activity involving watched category \"$1\""
msgstr ""

#. Substituted as $5 in {{msg-mw|categorywatch-emailbody}}.
#. * $1 is a page name
#. * $2 is the target category name
#. * $3 is the source category name
msgctxt "categorywatch-catmovein"
msgid "$1 has moved into $2 from $3"
msgstr ""

#. Substituted as $5 in {{msg-mw|categorywatch-emailbody}}.
#. * $1 is a page name
#. * $2 is the source category name
#. * $3 is the target category name
msgctxt "categorywatch-catmoveout"
msgid "$1 has moved out of $2 into $3"
msgstr ""

#. Substituted as $5 in {{msg-mw|categorywatch-emailbody}}.
#. * $1 is a page name
#. * $2 is a category name
msgctxt "categorywatch-catadd"
msgid "$1 has been added to $2"
msgstr ""

#. Substituted as $5 in {{msg-mw|categorywatch-emailbody}}.
#. * $1 is a page name
#. * $2 is a category name
msgctxt "categorywatch-catsub"
msgid "$1 has been removed from $2"
msgstr ""

#:
msgctxt "categorywatch-autocat"
msgid "Automatically watched by $1"
msgstr ""

