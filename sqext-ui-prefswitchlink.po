# Translation of Usability Initiative - Pref Switch Link to Albanian
msgid ""
msgstr ""
"Project-Id-Version: MediaWiki 1.17alpha (r66643)\n"
"Report-Msgid-Bugs-To: http://translatewiki.net\n"
"POT-Creation-Date: 2010-05-19 18:30:53+0000\n"
"Language-Team: Albanian\n"
"Content-Type: text-plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: MediaWiki Translate extension 2010-05-15\n"
"X-Language-Code: sq\n"
"X-Message-Group: ext-ui-prefswitchlink\n"

#. A link in the personal tools menu which takes users to a page where they can learn more about the new features.
msgctxt "prefswitch-link-anon"
msgid "New features"
msgstr ""

#:
msgctxt "tooltip-pt-prefswitch-link-anon"
msgid "Learn about new features"
msgstr ""

#. A link in the personal tools menu which takes users to a page where they can turn the new features off
msgctxt "prefswitch-link-on"
msgid "Take me back"
msgstr ""

#:
msgctxt "tooltip-pt-prefswitch-link-on"
msgid "Disable new features"
msgstr ""

#. A link in the personal tools menu which takes users to a page where they can turn the new features on
msgctxt "prefswitch-link-off"
msgid "New features"
msgstr ""

#:
msgctxt "tooltip-pt-prefswitch-link-off"
msgid "Try out new features"
msgstr ""

