# Translation of Editing Tips to Gheg Albanian
msgid ""
msgstr ""
"Project-Id-Version: MediaWiki 1.17alpha (r66640)\n"
"Report-Msgid-Bugs-To: http://translatewiki.net\n"
"POT-Creation-Date: 2010-05-19 17:50:05+0000\n"
"Language-Team: Gheg Albanian\n"
"Content-Type: text-plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: MediaWiki Translate extension 2010-05-15\n"
"X-Language-Code: aln\n"
"X-Message-Group: wikia-editingtips\n"

#:
msgctxt "tog-disableeditingtips"
msgid "Do not show editing tips"
msgstr ""

#:
msgctxt "tog-widescreeneditingtips"
msgid "Use widescreen editing"
msgstr ""

#:
msgctxt "editingtips_enter_widescreen"
msgid "Enter Widescreen"
msgstr ""

#:
msgctxt "editingtips_exit_widescreen"
msgid "Exit Widescreen"
msgstr ""

#:
msgctxt "editingtips_hide"
msgid "Hide Editing Tips"
msgstr ""

#:
msgctxt "editingtips_show"
msgid "Show Editing Tips"
msgstr ""

#:
msgctxt "editingTips"
msgid "Add your tips [[MediaWiki:EditingTips|here]]"
msgstr ""

