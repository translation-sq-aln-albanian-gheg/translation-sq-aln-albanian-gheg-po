<!doctype html>
<html lang=en dir=ltr>
<meta charset=utf-8>
<title>Internal error - translatewiki.net</title>
<meta name=generator content="MediaWiki 1.17alpha">
<meta name=robots content=noindex,nofollow>
<link rel="shortcut icon" href="/favicon.ico">
<link rel=search type="application/opensearchdescription+xml" href="/w/opensearch_desc.php" title="translatewiki.net (en)">
<link rel=alternate type="application/atom+xml" title="translatewiki.net Atom feed" href="/w/i.php?title=Special:RecentChanges&amp;feed=atom">
<link rel=stylesheet href="http://bits.translatewiki.net/w/skins/common/shared.css?281t5" media=screen>
<link rel=stylesheet href="http://bits.translatewiki.net/w/skins/modern/main.css?281t5" media=screen>
<link rel=stylesheet href="http://bits.translatewiki.net/w/skins/modern/print.css?281t5" media=print>
<link rel=stylesheet href="http://bits.translatewiki.net/w/extensions/Translate/Translate.css">
<link rel=stylesheet href="/w/i.php?title=MediaWiki:Common.css&amp;usemsgcache=yes&amp;ctype=text%2Fcss&amp;smaxage=18000&amp;action=raw&amp;maxage=18000">
<link rel=stylesheet href="/w/i.php?title=MediaWiki:Print.css&amp;usemsgcache=yes&amp;ctype=text%2Fcss&amp;smaxage=18000&amp;action=raw&amp;maxage=18000" media=print>
<link rel=stylesheet href="/w/i.php?title=MediaWiki:Modern.css&amp;usemsgcache=yes&amp;ctype=text%2Fcss&amp;smaxage=18000&amp;action=raw&amp;maxage=18000">
<link rel=stylesheet href="/w/i.php?title=-&amp;action=raw&amp;maxage=18000&amp;gen=css">
<script>
var skin="modern",
stylepath="http://bits.translatewiki.net/w/skins",
wgUrlProtocols="http\\:\\/\\/|https\\:\\/\\/|ftp\\:\\/\\/|irc\\:\\/\\/|gopher\\:\\/\\/|telnet\\:\\/\\/|nntp\\:\\/\\/|worldwind\\:\\/\\/|mailto\\:|news\\:|svn\\:\\/\\/|git\\:\\/\\/|mms\\:\\/\\/",
wgArticlePath="/wiki/$1",
wgScriptPath="/w",
wgScriptExtension=".php",
wgScript="/w/i.php",
wgVariantArticlePath=false,
wgActionPaths={},
wgServer="http://translatewiki.net",
wgCanonicalNamespace="Special",
wgCanonicalSpecialPageName="Translate",
wgNamespaceNumber=-1,
wgPageName="Special:Translate",
wgTitle="Translate",
wgAction="view",
wgArticleId=0,
wgIsArticle=false,
wgUserName=null,
wgUserGroups=["*"],
wgUserLanguage="en",
wgContentLanguage="en",
wgBreakFrames=false,
wgCurRevisionId=0,
wgVersion="1.17alpha",
wgEnableAPI=true,
wgEnableWriteAPI=true,
wgSeparatorTransformTable=["", ""],
wgDigitTransformTable=["", ""],
wgMainPageTitle="Main Page",
wgFormattedNamespaces={"-2": "Media", "-1": "Special", "0": "", "1": "Talk", "2": "User", "3": "User talk", "4": "Project", "5": "Project talk", "6": "File", "7": "File talk", "8": "MediaWiki", "9": "MediaWiki talk", "10": "Template", "11": "Template talk", "12": "Help", "13": "Help talk", "14": "Category", "15": "Category talk", "90": "Thread", "91": "Thread talk", "92": "Summary", "93": "Summary talk", "100": "Portal", "101": "Portal talk", "1102": "Translating", "1103": "Translating talk", "1198": "Translations", "1199": "Translations talk", "1200": "Voctrain", "1201": "Voctrain talk", "1202": "FreeCol", "1203": "FreeCol talk", "1204": "Nocc", "1205": "Nocc talk", "1206": "Wikimedia", "1207": "Wikimedia talk", "1208": "StatusNet", "1209": "StatusNet talk", "1210": "Mantis", "1211": "Mantis talk", "1212": "Mwlib", "1213": "Mwlib talk", "1214": "Commonist", "1215": "Commonist talk", "1216": "OpenLayers", "1217": "OpenLayers talk", "1218": "FUDforum", "1219": "FUDforum talk", "1220": "Okawix", "1221": "Okawix talk", "1222": "Osm", "1223": "Osm talk", "1224": "WikiReader", "1225": "WikiReader talk"},
wgNamespaceIds={"media": -2, "special": -1, "": 0, "talk": 1, "user": 2, "user_talk": 3, "project": 4, "project_talk": 5, "file": 6, "file_talk": 7, "mediawiki": 8, "mediawiki_talk": 9, "template": 10, "template_talk": 11, "help": 12, "help_talk": 13, "category": 14, "category_talk": 15, "thread": 90, "thread_talk": 91, "summary": 92, "summary_talk": 93, "portal": 100, "portal_talk": 101, "translating": 1102, "translating_talk": 1103, "translations": 1198, "translations_talk": 1199, "voctrain": 1200, "voctrain_talk": 1201, "freecol": 1202, "freecol_talk": 1203, "nocc": 1204, "nocc_talk": 1205, "wikimedia": 1206, "wikimedia_talk": 1207, "statusnet": 1208, "statusnet_talk": 1209, "mantis": 1210, "mantis_talk": 1211, "mwlib": 1212, "mwlib_talk": 1213, "commonist": 1214, "commonist_talk": 1215, "openlayers": 1216, "openlayers_talk": 1217, "fudforum": 1218, "fudforum_talk": 1219, "okawix": 1220, "okawix_talk": 1221, "osm": 1222, "osm_talk": 1223, "wikireader": 1224, "wikireader_talk": 1225, "betawiki": 4, "betawiki_talk": 5, "image": 6, "image_talk": 7},
wgSiteName="translatewiki.net",
wgCategories=[],
wgMWSuggestTemplate="http://translatewiki.net/w/api.php?action=opensearch\x26search={searchTerms}\x26namespace={namespaces}\x26suggest",
wgDBname="mediawiki",
wgSearchNamespaces=[0, 8],
wgMWSuggestMessages=["with suggestions", "no suggestions"],
wgRestrictionEdit=[],
wgRestrictionMove=[];
</script><script src="http://bits.translatewiki.net/w/skins/common/wikibits.js?281t5"></script>
<script src="http://bits.translatewiki.net/w/skins/common/jquery-1.4.2.min.js?281t5"></script>
<script src="http://bits.translatewiki.net/w/skins/common/ajax.js?281t5"></script>
<script src="http://bits.translatewiki.net/w/skins/common/mwsuggest.js?281t5"></script>
<script src="/w/i.php?title=-&amp;action=raw&amp;gen=js&amp;useskin=modern&amp;281t5"></script>

<body class="mediawiki ltr ns--1 ns-special page-Special_Translate skin-modern">

	<!-- heading -->
	<div id="mw_header"><h1 id="firstHeading">Internal error</h1></div>

	<div id="mw_main">
	<div id="mw_contentwrapper">
	<!-- navigation portlet -->
	<div id="p-cactions" class="portlet">
		<h5>Views</h5>
		<div class="pBody">
			<ul>
	
				 <li id="ca-nstab-special" class="selected"><a href="/w/i.php?title=Special%3ATranslate&amp;task=export-as-po&amp;group=wikia-editingtips&amp;language=sq&amp;limit=2500" title="This is a special page, you cannot edit the page itself">Special page</a></li>			</ul>
		</div>
	</div>

	<!-- content -->
	<div id="mw_content">
	<!-- contentholder does nothing by default, but it allows users to style the text inside
	     the content area without affecting the meaning of 'em' in #mw_content, which is used
	     for the margins -->
	<div id="mw_contentholder" >
		<div class='mw-topboxes'>
			<div id="mw-js-message" style="display:none;"></div>
			<div class="mw-topbox" id="siteSub">From translatewiki.net</div>
						<div class="mw-topbox" id="siteNotice"><div style="text-align:center;font-size:x-small;" class="plainlinks">
<p>2010-04-30: <span class="plainlinks"><i><a href="http://translatewiki.net/w/i.php?title=Special:Translate&amp;task=untranslated&amp;group=out-statusnet" class="external text" rel=nofollow>StatusNet</a></i></span> 0.9.2  will be released soon and can be <a href="http://status.net/wiki/Download" class="external text" rel=nofollow>downloaded</a>. Thank you translators for your contributions!<br/>
2010-04-30: translatewiki.net is looking for volunteer PHP developers to further improve the site. <a href="/wiki/Special:WebChat" title=Special:WebChat>Talk to us</a> if you can help out. (<a href="/wiki/Project:News" title=Project:News>Other news...</a>)
</p>
</div>
</div>
					</div>

		<div id="contentSub"></div>

				<div id="jump-to-nav">Jump to: <a href="#mw_portlets">navigation</a>, <a href="#searchInput">search</a></div>
		<p><br /><br /><br /><br />This message is logged, but you might want to report it on #mediawiki-i18n @ freenode or to user nike in this wiki.The command 'perl -MYAML::Syck=LoadFile -MPHP::Serialization=serialize -wle 'my $tf = q[/tmp/yaml-load-ySIM52];my $yaml = LoadFile($tf);open my $fh, &quot;&gt;&quot;, &quot;$tf.serialized&quot; or die qq[Can not open &quot;$tf.serialized&quot;];print $fh serialize($yaml);close($fh);' 2&gt;&amp;1' died in execution with exit code '2': Can't locate YAML/Syck.pm in @INC (@INC contains: /etc/perl /usr/local/lib/perl/5.10.0 /usr/local/share/perl/5.10.0 /usr/lib/perl5 /usr/share/perl5 /usr/lib/perl/5.10 /usr/share/perl/5.10 /usr/local/lib/site_perl .).
BEGIN failed--compilation aborted.
</p><p>Backtrace:</p><p>#0 /www/w/extensions/Translate/utils/TranslateYaml.php(52): wfDebugDieBacktrace('The command 'pe...')<br />
#1 /www/w/extensions/Translate/utils/TranslateYaml.php(11): TranslateYaml::syckLoad('---?BASIC:?  id...')<br />
#2 /www/w/extensions/Translate/utils/TranslateYaml.php(19): TranslateYaml::loadString('---?BASIC:?  id...')<br />
#3 /www/w/extensions/Translate/MessageGroups.php(763): TranslateYaml::load('/www/w/extensio...')<br />
#4 /www/w/extensions/Translate/MessageGroups.php(779): MessageGroups::init()<br />
#5 /www/w/extensions/Translate/TranslatePage.php(175): MessageGroups::getGroup('wikia-editingti...')<br />
#6 /www/w/extensions/Translate/TranslatePage.php(52): SpecialTranslate-&gt;setup(NULL)<br />
#7 /www/w/includes/SpecialPage.php(572): SpecialTranslate-&gt;execute(NULL)<br />
#8 /www/w/includes/Wiki.php(259): SpecialPage::executePath(Object(Title))<br />
#9 /www/w/includes/Wiki.php(67): MediaWiki-&gt;handleSpecialCases(Object(Title), Object(OutputPage), Object(WebRequest))<br />
#10 /www/w/index.php(116): MediaWiki-&gt;performRequestForTitle(Object(Title), NULL, Object(OutputPage), Object(User), Object(WebRequest))<br />
#11 {main}</p>
<div class="printfooter">
Retrieved from "<a href="http://translatewiki.net/wiki/Special:Translate">http://translatewiki.net/wiki/Special:Translate</a>"</div>
		<div class='mw_clear'></div>
		<div id='catlinks' class='catlinks catlinks-allhidden'></div>			</div><!-- mw_contentholder -->
	</div><!-- mw_content -->
	</div><!-- mw_contentwrapper -->

	<div id="mw_portlets">

	<!-- portlets -->
		<div id="p-search" class="portlet">
		<h5><label for="searchInput">Search</label></h5>
		<div id="searchBody" class="pBody">
			<form action="/w/i.php" id="searchform">
				<input type='hidden' name="title" value="Special:Search"/>
				<input id=searchInput title="Search translatewiki.net" accesskey=f type=search name=search>
				<input type='submit' name="go" class="searchButton" id="searchGoButton"	value="Go" title="Go to a page with this exact name if exists" />&nbsp;
				<input type='submit' name="fulltext" class="searchButton" id="mw-searchButton" value="Search" title="Search the pages for this text" />
			</form>
		</div>
	</div>
	<div class='generated-sidebar portlet' id='p-navigation'>
		<h5>Navigation</h5>
		<div class='pBody'>
			<ul>
				<li id="n-mainpage"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main Page</a></li>
				<li id="n-bw-sidebar-news"><a href="/wiki/Project:News">News</a></li>
				<li id="n-bw-sidebar-projects"><a href="/wiki/Project_list">Projects</a></li>
				<li id="n-bw-mainpage-support-title"><a href="/wiki/Support">Support</a></li>
				<li id="n-bw-sidebar-chat"><a href="/wiki/Special:WebChat">Chat</a></li>
			</ul>
		</div>
	</div>
	<div class='generated-sidebar portlet' id='p-bw-sidebar-translators'>
		<h5>Translators</h5>
		<div class='pBody'>
			<ul>
				<li id="n-bw-sidebar-intro"><a href="/wiki/Special:FirstSteps">Introduction</a></li>
				<li id="n-bw-sidebar-portal"><a href="/wiki/Portal:En">Portal for English</a></li>
				<li id="n-bw-sidebar-languages"><a href="/wiki/Translating:Languages">Maintained languages</a></li>
				<li id="n-bw-sidebar-translate"><a href="/wiki/Special:LanguageStats/en">Translation tool</a></li>
			</ul>
		</div>
	</div>
	<div class='generated-sidebar portlet' id='p-recentchanges'>
		<h5>Recent changes</h5>
		<div class='pBody'>
			<ul>
				<li id="n-bw-sidebar-changes-lang"><a href="http://translatewiki.net/w/i.php?title=Special:RecentChanges&amp;translations=only&amp;trailer=/en">Translations in English</a></li>
				<li id="n-bw-sidebar-changes-lang-all"><a href="http://translatewiki.net/w/i.php?title=Special:RecentChanges&amp;translations=only">All translations</a></li>
				<li id="n-bw-sidebar-changes-wiki"><a href="http://translatewiki.net/w/i.php?title=Special:RecentChanges&amp;translations=filter">Site changes</a></li>
			</ul>
		</div>
	</div>
	<div class="portlet" id="p-tb">
		<h5>Toolbox</h5>
		<div class="pBody">
			<ul>
<li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="List of all special pages [q]" accesskey="q">Special pages</a></li>
			</ul>
		</div>
	</div>
	<div class='generated-sidebar portlet' id='p-googleadsense'>
		<h5>Google AdSense</h5>
		<div class='pBody'>
<script type="text/javascript">
/* <![CDATA[ */
google_ad_client = "pub-8440046403419693";
/* translatewiki-2 */
google_ad_slot = "9240280893";
google_ad_width = 120;
google_ad_height = 240;
/* ]]> */
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>		</div>
	</div>

	</div><!-- mw_portlets -->


	</div><!-- main -->

	<div class="mw_clear"></div>

	<!-- personal portlet -->
	<div class="portlet" id="p-personal">
		<h5>Personal tools</h5>
		<div class="pBody">
			<ul>
				<li id="pt-login"><a href="/w/i.php?title=Special:UserLogin&amp;returnto=Special:Translate&amp;returntoquery=task%3Dexport-as-po%26group%3Dwikia-editingtips%26language%3Dsq%26limit%3D2500" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in / create account</a></li>
			</ul>
		</div>
	</div>


	<!-- footer -->
	<div id="footer">
			<ul id="f-list">
				<li id="privacy"><a href="/wiki/Project:Privacy_policy" title="Project:Privacy policy">Privacy policy</a></li>
				<li id="about"><a href="/wiki/Project:About" title=Project:About>About translatewiki.net</a></li>
				<li id="disclaimer"><a href="/wiki/Project:General_disclaimer" title="Project:General disclaimer">Disclaimers</a></li>
			</ul>
		<div class='mw_poweredby'><a href="http://www.netcup.de/" title="Powered by netcup - Webspace and vServer" target="_blank">Powered by netcup - Webspace and vServer</a></div>	</div>

	
<script>if (window.runOnloadHook) runOnloadHook();</script>
<!-- Served in 0.178 secs. --></body></html>
