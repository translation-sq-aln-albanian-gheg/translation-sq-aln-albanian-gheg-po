# Translation of Comment Spammer to Gheg Albanian
msgid ""
msgstr ""
"Project-Id-Version: MediaWiki 1.17alpha (r66640)\n"
"Report-Msgid-Bugs-To: http://translatewiki.net\n"
"POT-Creation-Date: 2010-05-19 17:11:03+0000\n"
"Language-Team: Gheg Albanian\n"
"Content-Type: text-plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: MediaWiki Translate extension 2010-05-15\n"
"X-Language-Code: aln\n"
"X-Message-Group: ext-commentspammer\n"

#:
msgctxt "commentspammer-save-blocked"
msgid "Your IP address is a suspected comment spammer, so the page has not been saved.\n[[Special:UserLogin|Log in or create an account]] to avoid this."
msgstr ""

#. Shown in [[Special:Version]]
msgctxt "commentspammer-desc"
msgid "Rejects edits from suspected comment spammers on a DNS blacklist"
msgstr ""

#:
msgctxt "commentspammer-log-msg"
msgid "edit from [[Special:Contributions/$1|$1]] to [[:$2]]."
msgstr ""

#:
msgctxt "commentspammer-log-msg-info"
msgid "Last spammed $1 {{PLURAL:$1|day|days}} ago, threat level is $2, and offence code is $3.\n[http://www.projecthoneypot.org/search_ip.php?ip=$4 View details], or [[Special:Blockip/$4|block]]."
msgstr ""

#:
msgctxt "cspammerlogpagetext"
msgid "Record of edits that have been allowed or denied based on whether the source was a known comment spammer."
msgstr ""

#:
msgctxt "cspammer-log-page"
msgid "Comment spammer log"
msgstr ""

