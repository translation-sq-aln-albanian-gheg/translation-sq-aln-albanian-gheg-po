# Translation of Flagged Revisions - Flagged Revs to Gheg Albanian
msgid ""
msgstr ""
"Project-Id-Version: MediaWiki 1.17alpha (r66640)\n"
"Report-Msgid-Bugs-To: http://translatewiki.net\n"
"POT-Creation-Date: 2010-05-19 17:16:52+0000\n"
"Language-Team: Gheg Albanian\n"
"Content-Type: text-plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: MediaWiki Translate extension 2010-05-15\n"
"X-Language-Code: aln\n"
"X-Message-Group: ext-flaggedrevs-flaggedrevs\n"

#. {{doc-action|review}}
msgctxt "action-review"
msgid "review revisions"
msgstr ""

#. {{Flagged Revs}}
#. {{Identical|Editor}}
msgctxt "editor"
msgid "Editor"
msgstr ""

#. {{Flagged Revs}}
#. General title for the [[Translating:Flagged Revs extension|Flagged Revs]] extension.
#. * "flagged" in the sense of "has been seen, has been checked"
msgctxt "flaggedrevs"
msgid "Flagged Revisions"
msgstr ""

#. {{Flagged Revs}}
msgctxt "flaggedrevs-backlog"
msgid "There is currently a backlog of [[Special:OldReviewedPages|pending changes]] to reviewed pages. '''Your attention is needed!'''"
msgstr ""

#. {{FlaggedRevs}}
#. Appears on top of watchlist and recent changes.
msgctxt "flaggedrevs-watched-pending"
msgid "There are currently [{{fullurl:{{#Special:OldReviewedPages}}|watched=1}} pending changes] to reviewed pages on your watchlist. '''Your attention is needed!'''"
msgstr ""

#. {{Flagged Revs}}
#. 
#. Shown in [[Special:Version]] as a short description of this extension. Do not translate links.
msgctxt "flaggedrevs-desc"
msgid "Gives Editors and Reviewers the ability to review revisions and stabilize pages"
msgstr ""

#. {{Flagged Revs-small}}
#. 
#. Shown in [[Special:Preferences]], under {{msg-mw|prefs-flaggedrevs}}, as a label for the alternative choices {{msg|flaggedrevs-pref-UI-0|pl=yes}}, and {{msg|flaggedrevs-pref-UI-1|pl=yes}}. See [[:Image:FlaggedRevs.jpg]] for an example image.
msgctxt "flaggedrevs-pref-UI"
msgid "Basic interface:"
msgstr ""

#. {{Flagged Revs-small}}
#. 
#. Option in [[Special:Preferences]], under {{msg-mw|prefs-flaggedrevs}}. See {{msg|flaggedrevs-pref-UI-1|pl=yes}} for the opposite message. See [[:Image:FlaggedRevs.jpg]] for an example image.
msgctxt "flaggedrevs-pref-UI-0"
msgid "Use detailed boxes to show approval status of pages"
msgstr ""

#. {{Flagged Revs-small}}
#. 
#. Option in [[Special:Preferences]], under {{msg-mw|prefs-flaggedrevs}}. See {{msg|flaggedrevs-pref-UI-0|pl=yes}} for the opposite message. See [[:Image:FlaggedRevs.jpg]] for an example image.
msgctxt "flaggedrevs-pref-UI-1"
msgid "Use small icons and minimal text to show approval status of pages"
msgstr ""

#. {{Flagged Revs-small}}
#. 
#. This appears in [[Special:Preferences]]:
#. * as an additional ''tab'', when JavaScript is enabled, or
#. * as an additional ''section header'', when JavaScript is disabled
msgctxt "prefs-flaggedrevs"
msgid "Edit approval"
msgstr ""

#. {{Flagged Revs}}
msgctxt "prefs-flaggedrevs-ui"
msgid "Edit approval"
msgstr ""

#. {{Flagged Revs}}
#. {{Identical|Content page}}
msgctxt "flaggedrevs-prefs-stable"
msgid "Always show the published version of content pages by default (if there is one)"
msgstr ""

#. {{Flagged Revs}}
msgctxt "flaggedrevs-prefs-watch"
msgid "Add pages I review to my watchlist"
msgstr ""

#. {{Flagged Revs}}
msgctxt "flaggedrevs-prefs-editdiffs"
msgid "Show the pending changes diff when editing pages"
msgstr ""

#. {{Flagged Revs}}
msgctxt "flaggedrevs-prefs-viewdiffs"
msgid "Show the pending changes diff when viewing the latest pending revision"
msgstr ""

#. {{Flagged Revs}}
msgctxt "group-editor"
msgid "Editors"
msgstr ""

#. {{Flagged Revs}}
#. {{Identical|Editor}}
msgctxt "group-editor-member"
msgid "editor"
msgstr ""

#. {{Flagged Revs}}
msgctxt "group-reviewer"
msgid "Reviewers"
msgstr ""

#. {{Flagged Revs}}
#. {{Identical|Reviewer}}
msgctxt "group-reviewer-member"
msgid "reviewer"
msgstr ""

#. {{Flagged Revs}}
msgctxt "grouppage-editor"
msgid "{{ns:project}}:Editor"
msgstr ""

#. {{Flagged Revs}}
msgctxt "grouppage-reviewer"
msgid "{{ns:project}}:Reviewer"
msgstr ""

#. {{Flagged Revs}}
msgctxt "group-autoreview"
msgid "Autochecked users"
msgstr ""

#. {{Flagged Revs}}
msgctxt "group-autoreview-member"
msgid "autochecked users"
msgstr ""

#. {{Flagged Revs}}
msgctxt "grouppage-autoreview"
msgid "{{ns:project}}:Autochecked users"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-hist-draft"
msgid "unchecked revision"
msgstr ""

#. {{Flagged Revs-small}}
#. The accuracy "quality", as displayed on the page history after a revision with this setting.
msgctxt "revreview-hist-quality"
msgid "quality revision"
msgstr ""

#. {{Flagged Revs-small}}
#. The accuracy "sighted", as displayed on the page history after a revision with this setting.
msgctxt "revreview-hist-basic"
msgid "checked revision"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-hist-quality-user"
msgid "[{{fullurl:$1|stableid=$2}} approved] by [[User:$3|$3]]"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-hist-basic-user"
msgid "[{{fullurl:$1|stableid=$2}} checked] by [[User:$3|$3]]"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-hist-basic-auto"
msgid "[{{fullurl:$1|stableid=$2}} automatically checked]"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-hist-quality-auto"
msgid "[{{fullurl:$1|stableid=$2}} automatically approved]"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-hist-pending"
msgid "'''[[{{fullurl:$1|oldid=$2&diff=$3}} pending review]]'''"
msgstr ""

#. {{Flagged Revs}}
msgctxt "review-edit-diff"
msgid "'''NOTICE: Some pending changes to the published version are incorporated into the edit form below.'''"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-diff-toggle-show"
msgid "show changes"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-diff-toggle-hide"
msgid "hide changes"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-diff-toggle-title"
msgid "Toggle display of pending changes to the published version"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-log-toggle-show"
msgid "show stability log"
msgstr ""

#:
msgctxt "revreview-log-toggle-hide"
msgid "hide stability log"
msgstr ""

#:
msgctxt "revreview-log-toggle-title"
msgid "Toggle display of stability settings log"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-log-details-show"
msgid "show details"
msgstr ""

#:
msgctxt "revreview-log-details-hide"
msgid "hide details"
msgstr ""

#:
msgctxt "revreview-log-details-title"
msgid "Toggle display of stability settings log"
msgstr ""

#. {{Flagged Revs}}
msgctxt "review-diff2stable"
msgid "View all pending changes"
msgstr ""

#. {{Flagged Revs}}
#. Parameters:
#. * $1 is a page title
msgctxt "review-logentry-app"
msgid "reviewed a version of [[$1]]"
msgstr ""

#. {{Flagged Revs}}
#. Parameters:
#. * $1 is a page title
msgctxt "review-logentry-dis"
msgid "deprecated a version of [[$1]]"
msgstr ""

#. {{Flagged Revs}}
msgctxt "review-logentry-id"
msgid "revision: $2"
msgstr ""

#. {{Flagged Revs}}
msgctxt "review-logentry-diff"
msgid "changes reviewed"
msgstr ""

#. {{Flagged Revs}}
msgctxt "review-logentry-diff2"
msgid "changes deprecated"
msgstr ""

#. {{Flagged Revs}}
msgctxt "review-logpage"
msgid "Review log"
msgstr ""

#. {{Flagged Revs}}
#. {{Identical|Content page}}
msgctxt "review-logpagetext"
msgid "This is a log of changes to revisions' [[{{MediaWiki:Validationpage}}|approval]] status for content pages."
msgstr ""

#. {{Flagged Revs}}
#. {{Identical|Reviewer}}
msgctxt "reviewer"
msgid "Reviewer"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revisionreview"
msgid "Review revisions"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-accuracy"
msgid "Accuracy"
msgstr ""

#. {{Flagged Revs-small}}
#. This is the default configuration, i.e. the revision has not (yet) been reviewed.
msgctxt "revreview-accuracy-0"
msgid "Unapproved"
msgstr ""

#. {{Flagged Revs-small}}
#. A basic check on vandalism ("sighted" as "has been seen/checked"). This configuration is considered as "flagged".
msgctxt "revreview-accuracy-1"
msgid "Sighted"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-accuracy-2"
msgid "Accurate"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-accuracy-3"
msgid "Well sourced"
msgstr ""

#. {{Flagged Revs}}
#. {{Identical|Featured}}
msgctxt "revreview-accuracy-4"
msgid "Featured"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-approved"
msgid "Approved"
msgstr ""

#. {{Flagged Revs}}
#. {{Identical|Automatic}}
msgctxt "revreview-auto"
msgid "(automatic)"
msgstr ""

#. {{Flagged Revs}}
#. * Parameter $2 is the date of the approval
msgctxt "revreview-basic"
msgid "This is the [[{{MediaWiki:Validationpage}}|published version]], [{{fullurl:{{#Special:Log}}|type=review&page={{FULLPAGENAMEE}}}} checked] on <i>$2</i>.\nThere {{PLURAL:$3|is|are}} [{{fullurl:{{FULLPAGENAMEE}}|oldid=$1&diff=cur&diffonly=0}} $3 pending {{PLURAL:$3|change|changes}}] awaiting review."
msgstr ""

#. {{Flagged Revs}}
#. * Parameter $2 is the date of the approval
msgctxt "revreview-basic-i"
msgid "This is the [[{{MediaWiki:Validationpage}}|published version]], [{{fullurl:{{#Special:Log}}|type=review&page={{FULLPAGENAMEE}}}} checked] on <i>$2</i>.\nThere are [{{fullurl:{{FULLPAGENAMEE}}|oldid=$1&diff=cur&diffonly=0}} template/file changes] awaiting review."
msgstr ""

#. {{Flagged Revs}}
#. * Parameter $2 is the date of the approval
msgctxt "revreview-basic-old"
msgid "This is a [[{{MediaWiki:Validationpage}}|checked]] version ([{{fullurl:{{#Special:ReviewedVersions}}|page={{FULLPAGENAMEE}}}} list all]), [{{fullurl:{{#Special:Log}}|type=review&page={{FULLPAGENAMEE}}}} approved] on <i>$2</i>.\n	New [{{fullurl:{{FULLPAGENAMEE}}|oldid=$1&diff=cur&diffonly=0}} changes] may have been made."
msgstr ""

#. {{Flagged Revs}}
#. * Parameter $2 is the date of the approval
msgctxt "revreview-basic-same"
msgid "This is the [[{{MediaWiki:Validationpage}}|published version]], [{{fullurl:{{#Special:Log}}|type=review&page={{FULLPAGENAMEE}}}} checked] on <i>$2</i>."
msgstr ""

#. {{Flagged Revs-small}}
#. Displayed on the top of a page when you are viewing an old sighted version. 
#. * Example: [http://de.wikipedia.org/w/index.php?title=Deutsche_Sprache&oldid=46894374 de.wikipedia].
#. * Parameter $2 is the date of the approval
msgctxt "revreview-basic-source"
msgid "A [{{fullurl:{{FULLPAGENAMEE}}|stableid=$1}} checked version] of this page, [{{fullurl:{{#Special:Log}}|type=review&page={{FULLPAGENAMEE}}}} approved] on <i>$2</i>, was based off this revision."
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-failed"
msgid "'''Unable to review this revision.''' The submission was incomplete or otherwise invalid."
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-changed"
msgid "'''The requested action could not be performed on this revision of [[:$1|$1]].'''\n\nA template or file may have been requested when no specific version was specified.\nThis can happen if a dynamic template transcludes another file or template depending on a variable that changed since you started reviewing this page.\nRefreshing the page and rereviewing can solve this problem."
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-current"
msgid "Pending changes"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-depth"
msgid "Depth"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-depth-0"
msgid "Unapproved"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-depth-1"
msgid "Basic"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-depth-2"
msgid "Moderate"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-depth-3"
msgid "High"
msgstr ""

#. {{Flagged Revs}}
#. {{Identical|Featured}}
msgctxt "revreview-depth-4"
msgid "Featured"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-draft-title"
msgid "Pending changes are displayed on this page"
msgstr ""

#. {{Flagged Revs-small}}
#. Users who see the stable version and not the draft version as page, have this message in the "edit" tab.
#. {{Identical|Edit}}
msgctxt "revreview-edit"
msgid "Edit"
msgstr ""

#. {{Flagged Revs}}
#. {{Identical|Authorised user}}
msgctxt "revreview-editnotice"
msgid "'''Your changes will be published once an authorized user reviews them. ([[{{MediaWiki:Validationpage}}|?]])'''"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-check-flag"
msgid "Publish currently pending changes"
msgstr ""

#. {{Flagged Revs-small}}
#. If an anonymous user edits a stable page, after saving the page he sees the draft version (<tt>stable=0</tt> in page title) he has made.
#. 
#. {{Identical|Authorised user}}
msgctxt "revreview-edited"
msgid "'''Changes will be published once an authorized user reviews them. ([[{{MediaWiki:Validationpage}}|?]])'''\n\nThere {{PLURAL:$2|is|are}} [{{fullurl:{{FULLPAGENAMEE}}|oldid=$1&diff=cur&diffonly=0}} $2 pending {{PLURAL:$2|change|changes}}] ''(shown below)'' awaiting review."
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-edited-section"
msgid "Return to page section named \"[[#$1|$2]]\"."
msgstr ""

#. {{Flagged Revs-small}}
#. * Title of the review box shown below a page (when you have the permission to review pages).
msgctxt "revreview-flag"
msgid "Review this revision"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-reflag"
msgid "Re-review this revision"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-invalid"
msgid "'''Invalid target:''' no [[{{MediaWiki:Validationpage}}|reviewed]] revision corresponds to the given ID."
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-legend"
msgid "Rate revision content"
msgstr ""

#. {{Flagged Revs}}
#. {{Identical|Comment}}
msgctxt "revreview-log"
msgid "Comment:"
msgstr ""

#. {{Flagged Revs}}
#. {{Identical|Content page}}
msgctxt "revreview-main"
msgid "You must select a particular revision of a content page in order to review.\n\nSee the [[Special:Unreviewedpages|list of unreviewed pages]]."
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-newest-basic"
msgid "The [{{fullurl:{{FULLPAGENAMEE}}|stable=1}} published version] was [{{fullurl:{{#Special:Log}}|type=review&page={{FULLPAGENAMEE}}}} checked] on <i>$2</i>. There {{PLURAL:$3|is|are}} [{{fullurl:{{FULLPAGENAMEE}}|oldid=$1&diff=cur&diffonly=0}} $3 pending {{PLURAL:$3|change|changes}}] awaiting review."
msgstr ""

#. {{Flagged Revs-small}}
#. Used in the "flagged revs box" when you are viewing the latest draft version, but when there is a sighted revision, the stable version. 
#. 
#. Example: [http://de.wikipedia.org/w/index.php?title=Deutsche_Sprache&stable=0 de.wikipedia].
#. * Note, the example seems not to work, currently.
msgctxt "revreview-newest-basic-i"
msgid "The [{{fullurl:{{FULLPAGENAMEE}}|stable=1}} published version] was [{{fullurl:{{#Special:Log}}|type=review&page={{FULLPAGENAMEE}}}} checked] on <i>$2</i>. There are [{{fullurl:{{FULLPAGENAMEE}}|oldid=$1&diff=cur&diffonly=0}} template/file changes] awaiting review."
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-newest-quality"
msgid "The [{{fullurl:{{FULLPAGENAMEE}}|stable=1}} published version] was [{{fullurl:{{#Special:Log}}|type=review&page={{FULLPAGENAMEE}}}} approved] on <i>$2</i>. There {{PLURAL:$3|is|are}} [{{fullurl:{{FULLPAGENAMEE}}|oldid=$1&diff=cur&diffonly=0}} $3 pending {{PLURAL:$3|change|changes}}] awaiting review."
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-newest-quality-i"
msgid "The [{{fullurl:{{FULLPAGENAMEE}}|stable=1}} published version] was [{{fullurl:{{#Special:Log}}|type=review&page={{FULLPAGENAMEE}}}} approved] on <i>$2</i>. There are [{{fullurl:{{FULLPAGENAMEE}}|oldid=$1&diff=cur&diffonly=0}} template/file changes] awaiting review."
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-pending-basic"
msgid "The [{{fullurl:{{FULLPAGENAMEE}}|stable=1}} published version] was [{{fullurl:{{#Special:Log}}|type=review&page={{FULLPAGENAMEE}}}} checked] on <i>$2</i>. There {{PLURAL:$3|is|are}} [{{fullurl:{{FULLPAGENAMEE}}|oldid=$1&diff=cur&diffonly=0}} $3 pending {{PLURAL:$3|change|changes}}] awaiting review."
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-pending-quality"
msgid "The [{{fullurl:{{FULLPAGENAMEE}}|stable=1}} published version] was [{{fullurl:{{#Special:Log}}|type=review&page={{FULLPAGENAMEE}}}} approved] on <i>$2</i>. There {{PLURAL:$3|is|are}} [{{fullurl:{{FULLPAGENAMEE}}|oldid=$1&diff=cur&diffonly=0}} $3 pending {{PLURAL:$3|change|changes}}] awaiting review."
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-pending-nosection"
msgid "Try viewing the [{{fullurl:{{FULLPAGENAMEE}}|stable=0}} latest revision], which includes\n[{{fullurl:{{FULLPAGENAMEE}}|oldid=$1&diff=cur&diffonly=0}} $2 pending {{PLURAL:$2|change|changes}}], to see if the section was moved or deleted."
msgstr ""

#. {{Flagged Revs-small}}
#. Shown above a page, when there are no reviewed revisions of that page.
msgctxt "revreview-noflagged"
msgid "There are no [[{{MediaWiki:Validationpage}}|reviewed]] revisions of this page, so it may '''not''' have been [[{{MediaWiki:Validationpage}}|checked]] for quality."
msgstr ""

#. {{Flagged Revs-small}}
#. Additionnal notes and comments, see [[:Image:FlaggedRevs2.png]].
msgctxt "revreview-note"
msgid "[[User:$1|$1]] made the following notes [[{{MediaWiki:Validationpage}}|reviewing]] this revision:"
msgstr ""

#. {{Flagged Revs-small}}
#. If <tt>$wgFlaggedRevsComments = true;</tt> there is a comment box to add any notes while reviewing the revision.
msgctxt "revreview-notes"
msgid "Observations or notes to display:"
msgstr ""

#. {{Flagged Revs-small}}
#. Used in the detailed version as label text for the ratings, see [http://translatewiki.net/w/images/8/84/FlaggedRevs.jpg this image] for an example.
msgctxt "revreview-oldrating"
msgid "It was rated:"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-quality"
msgid "This is the [[{{MediaWiki:Validationpage}}|published version]], [{{fullurl:{{#Special:Log}}|type=review&page={{FULLPAGENAMEE}}}} approved] on <i>$2</i>.\nThere {{PLURAL:$3|is|are}} [{{fullurl:{{FULLPAGENAMEE}}|oldid=$1&diff=cur&diffonly=0}} $3 pending {{PLURAL:$3|change|changes}}] awaiting review."
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-quality-i"
msgid "This is the [[{{MediaWiki:Validationpage}}|published version]], [{{fullurl:{{#Special:Log}}|type=review&page={{FULLPAGENAMEE}}}} approved] on <i>$2</i>.\nThere are [{{fullurl:{{FULLPAGENAMEE}}|oldid=$1&diff=cur&diffonly=0}} template/file changes] awaiting review."
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-quality-old"
msgid "This is a [[{{MediaWiki:Validationpage}}|quality]] version ([{{fullurl:{{#Special:ReviewedVersions}}|page={{FULLPAGENAMEE}}}} list all]), [{{fullurl:{{#Special:Log}}|type=review&page={{FULLPAGENAMEE}}}} approved] on <i>$2</i>.\n	New [{{fullurl:{{FULLPAGENAMEE}}|oldid=$1&diff=cur&diffonly=0}} changes] may have been made."
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-quality-same"
msgid "This is the [[{{MediaWiki:Validationpage}}|published version]], [{{fullurl:{{#Special:Log}}|type=review&page={{FULLPAGENAMEE}}}} approved] on <i>$2</i>."
msgstr ""

#. {{Flagged Revs-small}}
#. Displayed on the top of a page when you are viewing an old quality version. 
#. * Example: [http://de.wikipedia.org/w/index.php?title=Deutsche_Sprache&oldid=46894374 de.wikipedia] (this is a sighted version, but it's the same for a quality version).
#. * Parameter $2 is the date of the approval
msgctxt "revreview-quality-source"
msgid "A [{{fullurl:{{FULLPAGENAMEE}}|stableid=$1}} quality version] of this page, [{{fullurl:{{#Special:Log}}|type=review&page={{FULLPAGENAMEE}}}} approved] on <i>$2</i>, was based off this revision."
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-quality-title"
msgid "This is a quality version of this page"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-quick-basic"
msgid "'''[[{{MediaWiki:Validationpage}}|Checked]]''' [[{{fullurl:{{FULLPAGENAMEE}}|oldid=$1&diff=cur&diffonly=0}} review pending changes]]"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-quick-basic-old"
msgid "'''[[{{MediaWiki:Validationpage}}|Checked]]'''"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-quick-basic-same"
msgid "'''[[{{MediaWiki:Validationpage}}|Checked]]'''"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-quick-invalid"
msgid "'''Invalid revision ID'''"
msgstr ""

#. {{Flagged Revs-small}}
#. Shown in the "flagged revs box" on the content page when there isn't any sighted or quality revision yet.
msgctxt "revreview-quick-none"
msgid "'''[[{{MediaWiki:Validationpage}}|Unchecked]]'''"
msgstr ""

#. {{Flagged Revs-small}}
#. Used in the "flagged revs box" when viewing a quality version, while there are new changes (in a draft version) to be reviewed.
msgctxt "revreview-quick-quality"
msgid "'''[[{{MediaWiki:Validationpage}}|Quality]]''' [[{{fullurl:{{FULLPAGENAMEE}}|oldid=$1&diff=cur&diffonly=0}} review pending changes]]"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-quick-quality-old"
msgid "'''[[{{MediaWiki:Validationpage}}|Quality]]'''"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-quick-quality-same"
msgid "'''[[{{MediaWiki:Validationpage}}|Quality]]'''"
msgstr ""

#. {{Flagged Revs}}
#. {{Identical|Draft view article compare}}
msgctxt "revreview-quick-see-basic"
msgid "[[{{fullurl:{{FULLPAGENAMEE}}|oldid=$1&diff=cur&diffonly=0}} review pending changes]]"
msgstr ""

#. {{Flagged Revs}}
#. {{Identical|Draft view article compare}}
msgctxt "revreview-quick-see-quality"
msgid "[[{{fullurl:{{FULLPAGENAMEE}}|oldid=$1&diff=cur&diffonly=0}} review pending changes]]"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-selected"
msgid "Selected revision of '''$1:'''"
msgstr ""

#. {{Flagged Revs}}
#. {{Identical|View source}}
msgctxt "revreview-source"
msgid "View source"
msgstr ""

#. {{Flagged Revs}}
#. {{Identical|Stable}}
msgctxt "revreview-stable"
msgid "Published page"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-basic-title"
msgid "This is a checked version of this page"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-stable1"
msgid "You may want to view [{{fullurl:$1|stableid=$2}} this flagged version] and see if it is now the [{{fullurl:$1|stable=1}} published version] of this page."
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-stable2"
msgid "You may want to view the [{{fullurl:$1|stable=1}} published version] of this page."
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-style"
msgid "Readability"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-style-0"
msgid "Unapproved"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-style-1"
msgid "Acceptable"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-style-2"
msgid "Good"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-style-3"
msgid "Concise"
msgstr ""

#. {{Flagged Revs}}
#. {{Identical|Featured}}
msgctxt "revreview-style-4"
msgid "Featured"
msgstr ""

#. {{Flagged Revs-small}}
#. The text on the submit button in the form used to review pages.
#. 
#. {{Identical|Submit}}
msgctxt "revreview-submit"
msgid "Submit"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-submit-review"
msgid "Approve"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-submit-unreview"
msgid "De-approve"
msgstr ""

#. {{flaggedrevs}}
#. {{identical|submitting}}
msgctxt "revreview-submitting"
msgid "Submitting..."
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-submit-reviewed"
msgid "Done. Approved!"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-submit-unreviewed"
msgid "Done. De-approved!"
msgstr ""

#. {{Flagged Revs-small}}
#. Shown when a reviewer/editor has marked a revision as stable/sighted/... See also {{msg|revreview-successful2|pl=yes}}.
msgctxt "revreview-successful"
msgid "'''Revision of [[:$1|$1]] successfully flagged. ([{{fullurl:{{#Special:ReviewedVersions}}|page=$2}} view reviewed versions])'''"
msgstr ""

#. {{Flagged Revs-small}}
#. Shown when a reviewer/editor has marked a stable/sighted/... revision as unstable/unsighted/... After that, it can normally be reviewed again. See also {{msg|revreview-successful|pl=yes}}.
msgctxt "revreview-successful2"
msgid "'''Revision of [[:$1|$1]] successfully unflagged.'''"
msgstr ""

#. {{Flagged Revs-small}}
#. Displayed in the review box for the reviewers' information.
msgctxt "revreview-text"
msgid "''[[{{MediaWiki:Validationpage}}|Reviewed versions]] are checked versions of pages used to determine the published version.''"
msgstr ""

#. {{Flagged Revs-small}}
#. Tooltip shown when hovering over <span style="color:blue;">(-/+)</span>.
msgctxt "revreview-toggle-title"
msgid "show/hide details"
msgstr ""

#. {{Flagged Revs-small}}
#. A kind of error shown when trying to review a revision with all settings on "unapproved".
msgctxt "revreview-toolow"
msgid "'''You must rate each of the attributes higher than \"unapproved\" in order for a revision to be considered reviewed.'''\nTo remove the review status of a revision, set all fields to \"unapproved\".\n\nPlease hit the \"back\" button in your browser and try again."
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-update"
msgid "Please [[{{MediaWiki:Validationpage}}|review]] any pending changes ''(shown below)'' made to the published version."
msgstr ""

#:
msgctxt "revreview-update-edited"
msgid "<span class=\"flaggedrevs_important\">Your changes have not yet been published.</span>\n\nThere are previous edits pending review. To publish your changes, please review all the changes shown below."
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-update-includes"
msgid "'''Some templates/files were updated:'''"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-update-use"
msgid "'''NOTE:''' The published version of each of these templates/files is used in the published version of this page."
msgstr ""

#. {{Flagged Revs-small}}
#. Appears above the protection form when the current version of the page is the stable version; otherwise {{msg-mw|revreview-visibility2}} or {{msg-mw|revreview-visibility3}} is shown.
msgctxt "revreview-visibility"
msgid "'''This page has an updated [[{{MediaWiki:Validationpage}}|published version]]; page stability settings can be [{{fullurl:{{#Special:Stabilization}}|page={{FULLPAGENAMEE}}}} configured].'''"
msgstr ""

#. {{Flagged Revs-small}}
#. Appears on top of the protection form when the current version is not the stable version; otherwise {{msg-mw|revreview-visibility}} or {{msg-mw|revreview-visibility3}} is shown.
msgctxt "revreview-visibility2"
msgid "'''This page has an outdated [[{{MediaWiki:Validationpage}}|published version]]; page stability settings can be [{{fullurl:{{#Special:Stabilization}}|page={{FULLPAGENAMEE}}}} configured].'''"
msgstr ""

#. {{Flagged Revs-small}}
#. Appears on top of the protection form when the page has no stable version at all; otherwise {{msg-mw|revreview-visibility}} or {{msg-mw|revreview-visibility2}} is shown.
msgctxt "revreview-visibility3"
msgid "'''This page does not have a [[{{MediaWiki:Validationpage}}|published version]]; page stability settings can be [{{fullurl:{{#Special:Stabilization}}|page={{FULLPAGENAMEE}}}} configured].'''"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-revnotfound"
msgid "The old version of the page you asked for could not be found.\nPlease check the URL you used to access this page."
msgstr ""

#. {{Flagged Revs}}
#. 
#. {{doc-right|autoreview}}
msgctxt "right-autoreview"
msgid "Have one's own edits automatically marked as \"checked\""
msgstr ""

#. {{Flagged Revs}}
#. 
#. {{doc-right|movestable}}
msgctxt "right-movestable"
msgid "Move published pages"
msgstr ""

#. {{Flagged Revs}}
#. 
#. {{doc-right|review}}
msgctxt "right-review"
msgid "Mark revisions as being \"checked\""
msgstr ""

#. {{Flagged Revs}}
#. 
#. {{doc-right|stablesettings}}
msgctxt "right-stablesettings"
msgid "Configure how the published version is selected and displayed"
msgstr ""

#. {{Flagged Revs}}
#. 
#. {{doc-right}}
msgctxt "right-validate"
msgid "Mark revisions as being \"quality\""
msgstr ""

#. {{Flagged Revs}}
#. 
#. {{doc-right}}
msgctxt "right-unreviewedpages"
msgid "View the [[Special:UnreviewedPages|list of unreviewed pages]]"
msgstr ""

#. {{Flagged Revs}}
msgctxt "rights-editor-autosum"
msgid "autopromoted"
msgstr ""

#. {{Flagged Revs}}
msgctxt "rights-editor-revoke"
msgid "removed editor status from [[$1]]"
msgstr ""

#. {{Flagged Revs-small}}
#. A group in [[Special:SpecialPages]] for all special pages of the Flagged Revs extension.
msgctxt "specialpages-group-quality"
msgid "Edit approval"
msgstr ""

#. {{Flagged Revs}}
msgctxt "stable-logentry-config"
msgid "configured publication settings for [[$1]]"
msgstr ""

#. {{Flagged Revs}}
msgctxt "stable-logentry-reset"
msgid "reset publication settings for [[$1]]"
msgstr ""

#:
msgctxt "stable-log-restriction"
msgid "Publish: require \"$1\" permission"
msgstr ""

#. {{Flagged Revs}}
msgctxt "stable-logpage"
msgid "Stability log"
msgstr ""

#. {{Flagged Revs}}
#. {{Identical|Content pages}}
msgctxt "stable-logpagetext"
msgid "This is a log of changes to the [[{{MediaWiki:Validationpage}}|published version]] configuration of content pages."
msgstr ""

#. {{Flagged Revs}}
#. {{Identical|All}}
msgctxt "revreview-filter-all"
msgid "all"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-filter-stable"
msgid "published"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-statusfilter"
msgid "Approval action:"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-filter-approved"
msgid "Approved"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-filter-reapproved"
msgid "Re-approved"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-filter-unapproved"
msgid "De-approved"
msgstr ""

#. {{Flagged Revs}}
#. 
#. {{Identical|Type}}
msgctxt "revreview-typefilter"
msgid "Type:"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-filter-auto"
msgid "Automatic"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-filter-manual"
msgid "Manual"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-levelfilter"
msgid "Level:"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-precedencefilter"
msgid "Precedence:"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-lev-all"
msgid "any"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-lev-basic"
msgid "checked"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-lev-quality"
msgid "quality"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-lev-pristine"
msgid "pristine"
msgstr ""

#:
msgctxt "revreview-defaultfilter"
msgid "Default version:"
msgstr ""

#:
msgctxt "revreview-def-all"
msgid "any"
msgstr ""

#:
msgctxt "revreview-def-draft"
msgid "latest"
msgstr ""

#:
msgctxt "revreview-def-stable"
msgid "published"
msgstr ""

#:
msgctxt "revreview-restrictfilter"
msgid "Restriction level:"
msgstr ""

#:
msgctxt "revreview-restriction-any"
msgid "any"
msgstr ""

#:
msgctxt "revreview-restriction-none"
msgid "none"
msgstr ""

#. {{Flagged Revs}}
#. 
#. {{Identical|Review}}
msgctxt "revreview-reviewlink"
msgid "pending edits"
msgstr ""

#:
msgctxt "revreview-reviewlink-title"
msgid "View diff of all pending changes"
msgstr ""

#:
msgctxt "revreview-unreviewedpage"
msgid "unchecked page"
msgstr ""

#. {{Flagged Revs}}
msgctxt "tooltip-ca-current"
msgid "View this page with pending changes"
msgstr ""

#. {{Flagged Revs}}
msgctxt "tooltip-ca-stable"
msgid "View the published version of this page"
msgstr ""

#. {{Flagged Revs}}
msgctxt "tooltip-ca-default"
msgid "Quality assurance settings"
msgstr ""

#. {{FlaggedRevs}}
msgctxt "flaggedrevs-protect-legend"
msgid "Publish edits ([[{{MediaWiki:Validationpage}}|?]])"
msgstr ""

#. {{Flagged Revs}}
msgctxt "flaggedrevs-protect-none"
msgid "Allow all users"
msgstr ""

#. {{FlaggedRevs}}
msgctxt "flaggedrevs-protect-basic"
msgid "Default settings"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-locked-title"
msgid "Edits must be reviewed before being published on this page."
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-unlocked-title"
msgid "Edits do not require review before being published on this page."
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-locked"
msgid "'''Note:''' Edits must be [[{{MediaWiki:Validationpage}}|reviewed]] before being published on this page."
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-unlocked"
msgid "'''Note:''' Edits do not require [[{{MediaWiki:Validationpage}}|review]] before being published on this page."
msgstr ""

#. {{Flagged Revs}}
#. * $1 is one of {{msg|show}} or {{msg|hide}}
msgctxt "log-show-hide-review"
msgid "$1 review log"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-tt-review"
msgid "Apply this status to this revision"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-tt-flag"
msgid "Approve this revision by marking it as checked"
msgstr ""

#. {{Flagged Revs}}
msgctxt "revreview-tt-unflag"
msgid "De-approve this revision by marking it as unchecked"
msgstr ""

#. {{Flagged Revs-small}}
#. Link to the general help page. Do ''not'' translate the <tt><nowiki>{{ns:help}}:</nowiki></tt> part, including the colon.
msgctxt "validationpage"
msgid "{{ns:help}}:Page validation"
msgstr ""

