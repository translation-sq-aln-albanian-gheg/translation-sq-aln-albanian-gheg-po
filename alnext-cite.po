# Translation of Cite to Gheg Albanian
msgid ""
msgstr ""
"Project-Id-Version: MediaWiki 1.17alpha (r66640)\n"
"Report-Msgid-Bugs-To: http://translatewiki.net\n"
"POT-Creation-Date: 2010-05-19 17:09:53+0000\n"
"Language-Team: Gheg Albanian\n"
"Content-Type: text-plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: MediaWiki Translate extension 2010-05-15\n"
"X-Language-Code: aln\n"
"X-Message-Group: ext-cite\n"

#. Extension description for cite. Shown in [[Special:Version]]. Do not translate or change tag names.
msgctxt "cite-desc"
msgid "Adds <nowiki><ref[ name=id]></nowiki> and <nowiki><references/></nowiki> tags, for citations"
msgstr ""

#:
msgctxt "cite_croak"
msgid "Cite died; $1: $2"
msgstr ""

#. <tt>$str</tt> and <tt>$key</tt> are literals, and refers to who knows which variables the code uses.
msgctxt "cite_error_key_str_invalid"
msgid "Internal error;\ninvalid $str and/or $key.\nThis should never occur."
msgstr ""

#:
msgctxt "cite_error_stack_invalid_input"
msgid "Internal error;\ninvalid stack key.\nThis should never occur."
msgstr ""

#. Cite extension. This is used when there are errors in ref or references tags. The parameter $1 is an error message.
msgctxt "cite_error"
msgid "Cite error: $1"
msgstr ""

#. Cite extension. Error message shown if the name of a ref tag only contains digits. Examples that cause this error are <code>&lt;ref name="123" /&gt;</code> or <code>&lt;ref name="456"&gt;input&lt;/ref&gt;</code>
msgctxt "cite_error_ref_numeric_key"
msgid "Invalid <code>&lt;ref&gt;</code> tag;\nname cannot be a simple integer. Use a descriptive title"
msgstr ""

#. Cite extension. Error message shown when ref tags without any content (that is <code>&lt;ref/&gt;</code>) are used without a name.
msgctxt "cite_error_ref_no_key"
msgid "Invalid <code>&lt;ref&gt;</code> tag;\nrefs with no content must have a name"
msgstr ""

#. Cite extension. Error message shown when ref tags has parameters other than name. Examples that cause this error are <code>&lt;ref name="name" notname="value" /&gt;</code> or <code>&lt;ref notname="value" &gt;input&lt;ref&gt;</code>
msgctxt "cite_error_ref_too_many_keys"
msgid "Invalid <code>&lt;ref&gt;</code> tag;\ninvalid names, e.g. too many"
msgstr ""

#. Cite extension. Error message shown when ref tags without names have no content. An example that cause this error is <code>&lt;ref&gt;&lt;/ref&gt;</code>
msgctxt "cite_error_ref_no_input"
msgid "Invalid <code>&lt;ref&gt;</code> tag;\nrefs with no name must have content"
msgstr ""

#. Cite extension. Error message shown when parmeters are used in the references tag. An example that cause this error is <code>&lt;references someparameter="value" /&gt;</code>
msgctxt "cite_error_references_invalid_parameters"
msgid "Invalid <code>&lt;references&gt;</code> tag;\nno parameters are allowed.\nUse <code>&lt;references /&gt;</code>"
msgstr ""

#. Cite extension. Error message shown when unknown parameters are used in the references tag. An example that cause this error is <tt><nowiki><references someparameter="value" /></nowiki></tt>
msgctxt "cite_error_references_invalid_parameters_group"
msgid "Invalid <code>&lt;references&gt;</code> tag;\nparameter \"group\" is allowed only.\nUse <code>&lt;references /&gt;</code>, or <code>&lt;references group=\"...\" /&gt;</code>"
msgstr ""

#. Cite extension. Error message shown in the references tag when the same name is used for too many ref tags. Too many in this case is more than there are backlink labels defined in [[MediaWiki:Cite references link many format backlink labels]].
#. 
#. It is not possible to make a clickable link to this message. "nowiki" is mandatory around [[MediaWiki:Cite references link many format backlink labels]].
msgctxt "cite_error_references_no_backlink_label"
msgid "Ran out of custom backlink labels.\nDefine more in the <nowiki>[[MediaWiki:Cite references link many format backlink labels]]</nowiki> message"
msgstr ""

#. Cite extension. This error occurs when the tag <code>&lt;ref name="something" /&gt;</code> is used with the name-option specified and no other tag specifies a cite-text for this name.
msgctxt "cite_error_references_no_text"
msgid "Invalid <code>&lt;ref&gt;</code> tag;\nno text was provided for refs named <code>$1</code>"
msgstr ""

#. Error message shown if the <tt>&lt;ref&gt;</tt> tag is unbalanced, that means a <tt>&lt;ref&gt;</tt> is not followed by a <tt>&lt;/ref&gt;</tt>
msgctxt "cite_error_included_ref"
msgid "Closing &lt;/ref&gt; missing for &lt;ref&gt; tag"
msgstr ""

#:
msgctxt "cite_error_refs_without_references"
msgid "<code>&lt;ref&gt;</code> tags exist, but no <code>&lt;references/&gt;</code> tag was found"
msgstr ""

#:
msgctxt "cite_error_group_refs_without_references"
msgid "<code>&lt;ref&gt;</code> tags exist for a group named \"$1\", but no corresponding <code>&lt;references group=\"$1\"/&gt;</code> tag was found"
msgstr ""

#. Error message shown when doing something like
#. 
#. <pre>
#. <references group="foo">
#. <ref group="bar">...</ref>
#. </references>
#. </pre>
#. 
#. The <code>$1</code> is the value of the <code>group</code> attribute on the inner <code>&lt;ref&gt;</code> (in the example above, “bar”).
msgctxt "cite_error_references_group_mismatch"
msgid "<code>&lt;ref&gt;</code> tag in <code>&lt;references&gt;</code> has conflicting group attribute \"$1\"."
msgstr ""

#. Error message shown when doing something like
#. 
#. <pre>
#. <references group="foo">
#. <ref>...</ref>
#. </references>
#. </pre>
#. 
#. and there are no <code>&lt;ref&gt;</code> tags in the page text which would use <code>group="foo"</code>.
#. 
#. The <code>$1</code> is the name of the unused <code>group</code> (in the example above, “foo”).
msgctxt "cite_error_references_missing_group"
msgid "<code>&lt;ref&gt;</code> tag defined in <code>&lt;references&gt;</code> has group attribute \"$1\" which does not appear in prior text."
msgstr ""

#. Error message shown when using something like
#. 
#. <pre>
#. <references>
#. <ref name="refname">...</ref>
#. </references>
#. </pre>
#. 
#. and the reference <code>&lt;ref name="refname" /&gt;</code> is not used anywhere in the page text.
#. 
#. The <code>$1</code> parameter contains the name of the unused reference (in the example above, “refname”).
msgctxt "cite_error_references_missing_key"
msgid "<code>&lt;ref&gt;</code> tag with name \"$1\" defined in <code>&lt;references&gt;</code> is not used in prior text."
msgstr ""

#. Error message shown when a <code>&lt;ref&gt;</code> inside <code>&lt;references&gt;</code> does not have a <code>name</code> attribute.
msgctxt "cite_error_references_no_key"
msgid "<code>&lt;ref&gt;</code> tag defined in <code>&lt;references&gt;</code> has no name attribute."
msgstr ""

#. Error message shown when there is a <code><ref></code> inside <code><references></code>, but it does not have any content, e.g.
#. 
#. <pre>
#. <references>
#. <ref name="foo" />
#. </references>
#. </pre>
#. 
#. <code>$1</code> contains the <code>name</code> of the erroneous <code>&lt;ref&gt;</code> (in the above example, “foo”).
msgctxt "cite_error_empty_references_define"
msgid "<code>&lt;ref&gt;</code> tag defined in <code>&lt;references&gt;</code> with name \"$1\" has no content."
msgstr ""

