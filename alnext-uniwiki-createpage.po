# Translation of Uniwiki - Create Page to Gheg Albanian
msgid ""
msgstr ""
"Project-Id-Version: MediaWiki 1.17alpha (r66640)\n"
"Report-Msgid-Bugs-To: http://translatewiki.net\n"
"POT-Creation-Date: 2010-05-19 17:41:48+0000\n"
"Language-Team: Gheg Albanian\n"
"Content-Type: text-plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: MediaWiki Translate extension 2010-05-15\n"
"X-Language-Code: aln\n"
"X-Message-Group: ext-uniwiki-createpage\n"

#. Name of special page [[Special:CreatePage]] in [[Special:SpecialPages]] and its title.
#. {{Identical|Createpage}}
msgctxt "createpage"
msgid "Create a page"
msgstr "Krijo një faqe"

#. {{desc}}
msgctxt "createpage-desc"
msgid "Adds a [[Special:CreatePage|special page]] for creating new pages"
msgstr "Adds a [[Special: CreatePage faqe | speciale]] për të krijuar faqe të reja"

#. {{Identical|Submit}}
msgctxt "createpage_submitbutton"
msgid "Submit"
msgstr "Submit"

#:
msgctxt "createpage_instructions"
msgid "Enter the title of the page you wish to create:"
msgstr "Shkruani titullin e faqes që ju dëshironi për të krijuar:"

#:
msgctxt "createpage_entertitle"
msgid "Please enter a title for your page."
msgstr "Ju lutemi shkruani një titull për faqen tuaj."

#:
msgctxt "createpage_titleexists"
msgid "A page with the title [[$1]] already exists.\nWould you like to edit the existing page?"
msgstr "Një faqe me të [[$1 titullin]] ekziston. Doni të redaktuar faqe që ekziston?"

#:
msgctxt "createpage_tryagain"
msgid "No. I want to create a new page with a distinct title."
msgstr "Nr i duan të krijojnë një faqe të re me një titull të dallueshëm."

#:
msgctxt "createpage_editexisting"
msgid "Yes. I want to contribute to the existing page."
msgstr "Po. Unë dua të kontribuojë në faqe që ekziston."

#. Error message displayed in CreatePage special page when is specified an invalid title. See for example [[Special:CreatePage/]]
msgctxt "createpage-badtitle"
msgid "\"$1\" cannot be used as a page title"
msgstr "\"$1\" nuk mund të përdoret si një titull në faqe"

