# Translation of Widget Special Page to Gheg Albanian
msgid ""
msgstr ""
"Project-Id-Version: MediaWiki 1.17alpha (r66640)\n"
"Report-Msgid-Bugs-To: http://translatewiki.net\n"
"POT-Creation-Date: 2010-05-19 17:54:12+0000\n"
"Language-Team: Gheg Albanian\n"
"Content-Type: text-plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: MediaWiki Translate extension 2010-05-15\n"
"X-Language-Code: aln\n"
"X-Message-Group: wikia-widgetspecialpage\n"

#:
msgctxt "widgets-specialpage-info"
msgid "Widgets work best with one of the new skins, eg. [{{SERVER}}/index.php?title=Special:Widgets&useskin=monaco Monaco].\n\nPlease change [[Special:Preferences#prefsection-1|your preferences]] to use this tool."
msgstr ""

#:
msgctxt "widgets-specialpage-try-dashboard"
msgid "This page contains a list of all available widgets. To see your widgets at work go to [[Special:WidgetDashboard]]."
msgstr ""

