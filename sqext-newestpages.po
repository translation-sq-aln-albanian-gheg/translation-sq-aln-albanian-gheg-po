# Translation of Newest Pages to Albanian
msgid ""
msgstr ""
"Project-Id-Version: MediaWiki 1.17alpha (r66640)\n"
"Report-Msgid-Bugs-To: http://translatewiki.net\n"
"POT-Creation-Date: 2010-05-19 18:16:28+0000\n"
"Language-Team: Albanian\n"
"Content-Type: text-plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: MediaWiki Translate extension 2010-05-15\n"
"X-Language-Code: sq\n"
"X-Message-Group: ext-newestpages\n"

#:
msgctxt "newestpages"
msgid "Newest pages"
msgstr ""

#. Short description of this extension, shown on [[Special:Version]]. Do not translate or change links.
msgctxt "newestpages-desc"
msgid "Shows the [[Special:Newestpages|last X pages]] added to the wiki"
msgstr ""

#:
msgctxt "newestpages-header"
msgid "'''This page lists the {{PLURAL:$1|newest page|$1 newest pages}} on the wiki.'''"
msgstr ""

#. * $1 is a list like "10 | 20 | 30 | 50 |100 | 150"
msgctxt "newestpages-limitlinks"
msgid "Show up to $1 pages"
msgstr ""

#. {{Identical|Namespace}}
msgctxt "newestpages-namespace"
msgid "Namespace:"
msgstr ""

#:
msgctxt "newestpages-none"
msgid "No entries were found."
msgstr ""

#:
msgctxt "newestpages-ns-header"
msgid "'''This page lists the {{PLURAL:$1|newest page|$1 newest pages}} in the $2 namespace.'''"
msgstr ""

#:
msgctxt "newestpages-showing"
msgid "Listing {{PLURAL:$1|newest page|$1 newest pages}}:"
msgstr ""

#. {{Identical|Go}}
msgctxt "newestpages-submit"
msgid "Go"
msgstr ""

#:
msgctxt "newestpages-showredir"
msgid "Show redirect pages"
msgstr ""

#:
msgctxt "newestpages-hideredir"
msgid "Hide redirect pages"
msgstr ""

