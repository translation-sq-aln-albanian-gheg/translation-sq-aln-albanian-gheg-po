# Translation of Replace Text to Albanian
msgid ""
msgstr ""
"Project-Id-Version: MediaWiki 1.17alpha (r66640)\n"
"Report-Msgid-Bugs-To: http://translatewiki.net\n"
"POT-Creation-Date: 2010-05-19 18:22:32+0000\n"
"Language-Team: Albanian\n"
"Content-Type: text-plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: MediaWiki Translate extension 2010-05-15\n"
"X-Language-Code: sq\n"
"X-Message-Group: ext-replacetext\n"

#. This message is displayed as a title of this extension's special page.
msgctxt "replacetext"
msgid "Replace text"
msgstr ""

#. {{desc}}
#. 
#. {{Identical|Content page}}
msgctxt "replacetext-desc"
msgid "Provides a [[Special:ReplaceText|special page]] to allow administrators to do a global string find-and-replace on all the content pages of a wiki"
msgstr ""

#. Description of how to use this extension, displayed on the extension's special page ([[Special:ReplaceText]]).
msgctxt "replacetext_docu"
msgid "To replace one text string with another across all regular pages on this wiki, enter the two pieces of text here and then hit 'Continue'.\nYou will then be shown a list of pages that contain the search text, and you can choose the ones in which you want to replace it.\nYour name will appear in page histories as the user responsible for any changes."
msgstr ""

#. Label of the text field, where user enters original piece of text, which would be replaced.
msgctxt "replacetext_originaltext"
msgid "Original text:"
msgstr ""

#:
msgctxt "replacetext_replacementtext"
msgid "Replacement text:"
msgstr ""

#:
msgctxt "replacetext_optionalfilters"
msgid "Optional filters:"
msgstr ""

#:
msgctxt "replacetext_categorysearch"
msgid "Replace only in category:"
msgstr ""

#:
msgctxt "replacetext_prefixsearch"
msgid "Replace only in pages with the prefix:"
msgstr ""

#:
msgctxt "replacetext_editpages"
msgid "Replace text in page contents"
msgstr ""

#:
msgctxt "replacetext_movepages"
msgid "Replace text in page titles, when possible"
msgstr ""

#:
msgctxt "replacetext_givetarget"
msgid "You must specify the string to be replaced."
msgstr ""

#:
msgctxt "replacetext_nonamespace"
msgid "You must select at least one namespace."
msgstr ""

#:
msgctxt "replacetext_editormove"
msgid "You must select at least one of the replacement options."
msgstr ""

#. Displayed over the list of pages where the given text was found.
msgctxt "replacetext_choosepagesforedit"
msgid "Replace \"$1\" with \"$2\" in the text of the following {{PLURAL:$3|page|pages}}:"
msgstr ""

#:
msgctxt "replacetext_choosepagesformove"
msgid "Replace \"$1\" with \"$2\" in the {{PLURAL:$3|title of the following page|titles of the following pages}}:"
msgstr ""

#:
msgctxt "replacetext_cannotmove"
msgid "The following {{PLURAL:$1|page|pages}} cannot be moved:"
msgstr ""

#:
msgctxt "replacetext_formovedpages"
msgid "For moved pages:"
msgstr ""

#:
msgctxt "replacetext_savemovedpages"
msgid "Save the old titles as redirects to the new titles"
msgstr ""

#:
msgctxt "replacetext_watchmovedpages"
msgid "Watch these pages"
msgstr ""

#:
msgctxt "replacetext_invertselections"
msgid "Invert selections"
msgstr ""

#. Label of the button, which triggers the begin of replacment.
#. 
#. {{Identical|Replace}}
msgctxt "replacetext_replace"
msgid "Replace"
msgstr ""

#:
msgctxt "replacetext_success"
msgid "\"$1\" will be replaced with \"$2\" in $3 {{PLURAL:$3|page|pages}}."
msgstr ""

#:
msgctxt "replacetext_noreplacement"
msgid "No pages were found containing the string \"$1\"."
msgstr ""

#:
msgctxt "replacetext_nomove"
msgid "No pages were found whose title contains \"$1\"."
msgstr ""

#:
msgctxt "replacetext_nosuchcategory"
msgid "No category exists with the name \"$1\"."
msgstr ""

#:
msgctxt "replacetext_return"
msgid "Return to form."
msgstr ""

#:
msgctxt "replacetext_warning"
msgid "'''Warning:''' There {{PLURAL:$1|is $1 page that already contains|are $1 pages that already contain}} the replacement string, \"$2\". If you make this replacement you will not be able to separate your replacements from these strings."
msgstr ""

#:
msgctxt "replacetext_blankwarning"
msgid "'''Warning:''' Because the replacement string is blank, this operation will not be reversible."
msgstr ""

#. {{Identical|Continue}}
msgctxt "replacetext_continue"
msgid "Continue"
msgstr ""

#:
msgctxt "replacetext_cancel"
msgid "(Click the \"Back\" button in your browser to cancel the operation.)"
msgstr ""

#:
msgctxt "replacetext_editsummary"
msgid "Text replace - \"$1\" to \"$2\""
msgstr ""

#. {{doc-right}}
msgctxt "right-replacetext"
msgid "Do string replacements on the entire wiki"
msgstr ""

