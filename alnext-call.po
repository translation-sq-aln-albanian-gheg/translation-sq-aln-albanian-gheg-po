# Translation of Call to Gheg Albanian
msgid ""
msgstr ""
"Project-Id-Version: MediaWiki 1.17alpha (r66640)\n"
"Report-Msgid-Bugs-To: http://translatewiki.net\n"
"POT-Creation-Date: 2010-05-19 17:08:22+0000\n"
"Language-Team: Gheg Albanian\n"
"Content-Type: text-plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: MediaWiki Translate extension 2010-05-15\n"
"X-Language-Code: aln\n"
"X-Message-Group: ext-call\n"

#:
msgctxt "call"
msgid "Call"
msgstr ""

#. Short description of this extension, shown on [[Special:Version]]. Do not translate or change links.
msgctxt "call-desc"
msgid "Create a hyperlink to a template (or to a normal wiki page) with parameter passing.\nCan be used at the browser’s command line or within wiki text"
msgstr ""

#:
msgctxt "call-text"
msgid "The Call extension expects a wiki page and optional parameters for that page as an argument.<br /><br />\n\nExample 1: &nbsp; <tt>[[{{#special:call}}/My Template,parm1=value1]]</tt><br />\nExample 2: &nbsp; <tt>[[{{#special:call}}/Talk:My Discussion,parm1=value1]]</tt><br />\nExample 3: &nbsp; <tt>[[{{#special:call}}/:My Page,parm1=value1,parm2=value2]]</tt><br />\nExample 4 (Browser URL): &nbsp; <tt>http://mydomain/mywiki/index.php?{{#special:call}}/:My Page,parm1=value1</tt><br /><br />\n\nThe <i>Call extension</i> will call the given page and pass the parameters.<br />\nYou will see the contents of the called page and its title but its 'type' will be that of a special page, i.e. such a page cannot be edited.<br />The contents you see may vary depending on the value of the parameters you passed.<br /><br />\n\nThe <i>Call extension</i> is useful to build interactive applications with MediaWiki.<br />\nFor an example see <a href='http://semeb.com/dpldemo/Template:Catlist'>the DPL GUI</a> ..<br />\nIn case of problems you can try <b>{{#special:call}}/DebuG</b>"
msgstr ""

#:
msgctxt "call-save"
msgid "The output of this call would be saved to a page called ''$1''."
msgstr ""

#:
msgctxt "call-save-success"
msgid "The following text has been saved to page <big>[[$1]]</big> ."
msgstr ""

#:
msgctxt "call-save-failed"
msgid "The following text has NOT been saved to page <big>[[$1]]</big> because that page already exists."
msgstr ""

