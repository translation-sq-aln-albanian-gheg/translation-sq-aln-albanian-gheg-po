# Translation of Category Tree to Albanian
msgid ""
msgstr ""
"Project-Id-Version: MediaWiki 1.17alpha (r66640)\n"
"Report-Msgid-Bugs-To: http://translatewiki.net\n"
"POT-Creation-Date: 2010-05-19 17:59:29+0000\n"
"Language-Team: Albanian\n"
"Content-Type: text-plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: MediaWiki Translate extension 2010-05-15\n"
"X-Language-Code: sq\n"
"X-Message-Group: ext-categorytree\n"

#. Title of [[Special:CategoryTree]]
msgctxt "categorytree"
msgid "Category tree"
msgstr "Pema e kategorive"

#. {{Identical|Categories}}
#. 
#. Title for the CategoryPortlet, when shown in the side bar
msgctxt "categorytree-portlet"
msgid "Categories"
msgstr ""

#. Legend of the fieldset around the input form of [[Special:Categorytree]].
msgctxt "categorytree-legend"
msgid "Show category tree"
msgstr ""

#. Short description of the CategoryTree extension, shown on [[Special:Version]]
msgctxt "categorytree-desc"
msgid "Dynamically navigate the [[Special:CategoryTree|category structure]]"
msgstr ""

#. Header-text shown on [[Special:CategoryTree]]
msgctxt "categorytree-header"
msgid "Enter a category name to see its contents as a tree structure.\nNote that this requires advanced JavaScript functionality known as AJAX.\nIf you have a very old browser, or have JavaScript disabled, it will not work."
msgstr "Fusni emrin e Kategorisë për të parë Nënkategoritë si Pemë kategorish. Këtij funksioni i nevoiten JavaScript dhe AJAX për të funksionuar si duhet. Nëse keni një shfletues të vjetër, ose nëse i keni deaktivuar JavaScript kjo nuk do të funksionoj."

#. {{Identical|Category}}
#. 
#. Label for the category input field on Special:CategoryTree
msgctxt "categorytree-category"
msgid "Category:"
msgstr "Kategoria:"

#. Label for the submit button on [[Special:CategoryTree]]
msgctxt "categorytree-go"
msgid "Show tree"
msgstr "Plotëso"

#. Label for the list of parent categories on [[Special:CategoryTree]]
msgctxt "categorytree-parents"
msgid "Parents"
msgstr "Kryekategoritë"

#. Item for the mode choice on [[Special:CategoryTree]], indicating that only categories are listed
msgctxt "categorytree-mode-categories"
msgid "categories only"
msgstr "vetëm kategoritë"

#. Item for the mode choice on [[Special:CategoryTree]], indicating that no images in categories are listed
msgctxt "categorytree-mode-pages"
msgid "pages except files"
msgstr "faqet pa figurat"

#. Item for the mode choice on [[Special:CategoryTree]], indicating that all pages are listed.
#. 
#. {{Identical|All pages}}
msgctxt "categorytree-mode-all"
msgid "all pages"
msgstr "të gjitha faqet"

#. Tooltip for the "collapse" button
msgctxt "categorytree-collapse"
msgid "collapse"
msgstr "mbylle"

#. Tooltip for the "expand" button
msgctxt "categorytree-expand"
msgid "expand"
msgstr "hape"

#. Tooltip showing a detailed summary of subcategory member counts. Parameters:
#. * $1 = number of subcategories,
#. * $2 = number of pages (without subcategories and files),
#. * $3 = number of files,
#. * $4 = total number of members,
#. * $5 = members to be shown in the tree, depending on mode.
#. Use with { {PLURAL} }
msgctxt "categorytree-member-counts"
msgid "contains {{PLURAL:$1|1 subcategory|$1 subcategories}}, {{PLURAL:$2|1 page|$2 pages}}, and {{PLURAL:$3|1 file|$3 files}}"
msgstr ""

#. {{Identical|Load}}
#. 
#. Tooltip for the "expend" button, if the content was not yet loaded
msgctxt "categorytree-load"
msgid "load"
msgstr "hape"

#. {{Identical|Loading}}
#. 
#. Status message shown while loading content
msgctxt "categorytree-loading"
msgid "loading…"
msgstr "duke plotësuar"

#. Indicates items with matching criteria have been found
msgctxt "categorytree-nothing-found"
msgid "nothing found"
msgstr "Ju kërkoj ndjesë, nuk u gjet asgjë."

#. Indicates that there are no subcategories to be shown
msgctxt "categorytree-no-subcategories"
msgid "no subcategories"
msgstr "Asnjë nënkategori."

#. Indicates that there are no parent categories to be shown
msgctxt "categorytree-no-parent-categories"
msgid "no parent categories"
msgstr ""

#. Indicates that there are no pages or subcategories to be shown
msgctxt "categorytree-no-pages"
msgid "no pages or subcategories"
msgstr "Asnjë artikull ose nënkategori."

#. Indicates that the given category ($1) was not found
msgctxt "categorytree-not-found"
msgid "Category <i>$1</i> not found"
msgstr "Kategoria <i>$1</i> nuk u gjet"

#. Indicates that an error has occurred while loading the node's content
msgctxt "categorytree-error"
msgid "Problem loading data."
msgstr ""

#. Instruction to try again later
msgctxt "categorytree-retry"
msgid "Please wait a moment and try again."
msgstr ""

