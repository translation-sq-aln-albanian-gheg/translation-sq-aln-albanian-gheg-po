# Translation of ABC to Gheg Albanian
msgid ""
msgstr ""
"Project-Id-Version: MediaWiki 1.17alpha (r66640)\n"
"Report-Msgid-Bugs-To: http://translatewiki.net\n"
"POT-Creation-Date: 2010-05-19 17:05:48+0000\n"
"Language-Team: Gheg Albanian\n"
"Content-Type: text-plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: MediaWiki Translate extension 2010-05-15\n"
"X-Language-Code: aln\n"
"X-Message-Group: ext-abc\n"

#. Short description of the ABC extension, shown in [[Special:Version]]. Do not translate or change links.
msgctxt "abc-desc"
msgid "Adds <code>&lt;abc&gt;</code> tag to format ABC music"
msgstr "Shton tag'un <code>&lt;abc&gt;</code> me formatu muzikën ABC"

#. {{Identical|Download}}
msgctxt "abcdownload"
msgid "Download:"
msgstr "Shkarko:"

