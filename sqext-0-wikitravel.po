# Translation of Extensions used by Wikitravel to Albanian
msgid ""
msgstr ""
"Project-Id-Version: MediaWiki 1.17alpha (r66640)\n"
"Report-Msgid-Bugs-To: http://translatewiki.net\n"
"POT-Creation-Date: 2010-05-19 17:56:15+0000\n"
"Language-Team: Albanian\n"
"Content-Type: text-plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: MediaWiki Translate extension 2010-05-15\n"
"X-Language-Code: sq\n"
"X-Message-Group: ext-0-wikitravel\n"

#. Short description of the extension. Shown in [[Special:Version]].
msgctxt "stringfunctions-desc"
msgid "Enhances the parser with string functions"
msgstr ""

#:
msgctxt "renameuser"
msgid "Rename user"
msgstr "Ndërrim përdoruesi"

#. Link description used on Special:Contributions and Special:DeletedContributions. Only added if a user has rights to rename users.
msgctxt "renameuser-linkoncontribs"
msgid "rename user"
msgstr ""

#. Tooltip for {{msg-mw|renameuser-linkoncontribs}}.
msgctxt "renameuser-linkoncontribs-text"
msgid "Rename this user"
msgstr ""

#. Short description of the Renameuser extension, shown on [[Special:Version]]. Do not translate or change links.
msgctxt "renameuser-desc"
msgid "Adds a [[Special:Renameuser|special page]] to rename a user (need ''renameuser'' right)"
msgstr ""

#:
msgctxt "renameuserold"
msgid "Current username:"
msgstr "Emri i tanishëm"

#:
msgctxt "renameusernew"
msgid "New username:"
msgstr "Emri i ri"

#:
msgctxt "renameuserreason"
msgid "Reason for rename:"
msgstr ""

#:
msgctxt "renameusermove"
msgid "Move user and talk pages (and their subpages) to new name"
msgstr "Zhvendos faqet e përdoruesit dhe të diskutimit (dhe nën-faqet e tyre) tek emri i ri"

#. Option to block the old username (after it has been renamed) from being used again.
msgctxt "renameuserreserve"
msgid "Block the old username from future use"
msgstr ""

#. {{Identical|Warning}}
msgctxt "renameuserwarnings"
msgid "Warnings:"
msgstr ""

#:
msgctxt "renameuserconfirm"
msgid "Yes, rename the user"
msgstr ""

#. {{Identical|Submit}}
msgctxt "renameusersubmit"
msgid "Submit"
msgstr "Ndryshoje"

#:
msgctxt "renameusererrordoesnotexist"
msgid "The user \"<nowiki>$1</nowiki>\" does not exist."
msgstr "Përdoruesi me emër \"<nowiki>$1</nowiki>\" nuk ekziston"

#:
msgctxt "renameusererrorexists"
msgid "The user \"<nowiki>$1</nowiki>\" already exists."
msgstr "Përdoruesi me emër \"<nowiki>$1</nowiki>\" ekziston"

#:
msgctxt "renameusererrorinvalid"
msgid "The username \"<nowiki>$1</nowiki>\" is invalid."
msgstr "Emri \"<nowiki>$1</nowiki>\" nuk është i lejuar"

#:
msgctxt "renameusererrortoomany"
msgid "The user \"<nowiki>$1</nowiki>\" has $2 {{PLURAL:$2|contribution|contributions}}, renaming a user with more than $3 {{PLURAL:$3|contribution|contributions}} could adversely affect site performance."
msgstr "Përdoruesi \"<nowiki>$1</nowiki>\" ka dhënë $2 {{PLURAL:$2|kontribut|kontribute}}. Ndryshimi i emrit të një përdoruesi me më shumë se $3 {{PLURAL:$3|kontribut|kontribute}} mund të ndikojë rëndë tek rendimenti i shërbyesave."

#:
msgctxt "renameuser-error-request"
msgid "There was a problem with receiving the request.\nPlease go back and try again."
msgstr ""

#:
msgctxt "renameuser-error-same-user"
msgid "You cannot rename a user to the same thing as before."
msgstr ""

#:
msgctxt "renameusersuccess"
msgid "The user \"<nowiki>$1</nowiki>\" has been renamed to \"<nowiki>$2</nowiki>\"."
msgstr "Përdoruesi \"<nowiki>$1</nowiki>\" u riemërua në \"<nowiki>$2</nowiki>\""

#:
msgctxt "renameuser-page-exists"
msgid "The page $1 already exists and cannot be automatically overwritten."
msgstr ""

#:
msgctxt "renameuser-page-moved"
msgid "The page $1 has been moved to $2."
msgstr ""

#:
msgctxt "renameuser-page-unmoved"
msgid "The page $1 could not be moved to $2."
msgstr ""

#:
msgctxt "renameuserlogpage"
msgid "User rename log"
msgstr "Regjistri i emër-ndryshimeve"

#:
msgctxt "renameuserlogpagetext"
msgid "This is a log of changes to user names."
msgstr "Ky është një regjistër i ndryshimeve së emrave të përdoruesve"

#. Used in [[Special:Log/renameuser]].
#. * Parameter $1 is the original username
#. * Parameter $2 is the new username
msgctxt "renameuserlogentry"
msgid "renamed $1 to \"$2\""
msgstr ""

#:
msgctxt "renameuser-log"
msgid "{{PLURAL:$1|1 edit|$1 edits}}. Reason: $2"
msgstr "{{PLURAL:$1|1 redaktim|$1 redaktime}}. Arsyeja: $2"

#:
msgctxt "renameuser-move-log"
msgid "Automatically moved page while renaming the user \"[[User:$1|$1]]\" to \"[[User:$2|$2]]\""
msgstr ""

#. {{doc-right}}
msgctxt "right-renameuser"
msgid "Rename users"
msgstr ""

#. This message supports the use of GENDER with parameter $1.
msgctxt "renameuser-renamed-notice"
msgid "This user has been renamed.\nThe rename log is provided below for reference."
msgstr ""

#. Short description of the ParserFunctions extension, shown on [[Special:Version]].
msgctxt "pfunc_desc"
msgid "Enhance parser with logical functions"
msgstr ""

#:
msgctxt "pfunc_time_error"
msgid "Error: invalid time"
msgstr ""

#:
msgctxt "pfunc_time_too_long"
msgid "Error: too many #time calls"
msgstr ""

#:
msgctxt "pfunc_rel2abs_invalid_depth"
msgid "Error: Invalid depth in path: \"$1\" (tried to access a node above the root node)"
msgstr ""

#:
msgctxt "pfunc_expr_stack_exhausted"
msgid "Expression error: Stack exhausted"
msgstr ""

#:
msgctxt "pfunc_expr_unexpected_number"
msgid "Expression error: Unexpected number"
msgstr ""

#:
msgctxt "pfunc_expr_preg_match_failure"
msgid "Expression error: Unexpected preg_match failure"
msgstr ""

#:
msgctxt "pfunc_expr_unrecognised_word"
msgid "Expression error: Unrecognised word \"$1\""
msgstr ""

#:
msgctxt "pfunc_expr_unexpected_operator"
msgid "Expression error: Unexpected $1 operator"
msgstr ""

#:
msgctxt "pfunc_expr_missing_operand"
msgid "Expression error: Missing operand for $1"
msgstr ""

#:
msgctxt "pfunc_expr_unexpected_closing_bracket"
msgid "Expression error: Unexpected closing bracket"
msgstr ""

#:
msgctxt "pfunc_expr_unrecognised_punctuation"
msgid "Expression error: Unrecognised punctuation character \"$1\""
msgstr ""

#:
msgctxt "pfunc_expr_unclosed_bracket"
msgid "Expression error: Unclosed bracket"
msgstr ""

#. {{Identical|Divizion by zero}}
msgctxt "pfunc_expr_division_by_zero"
msgid "Division by zero"
msgstr ""

#:
msgctxt "pfunc_expr_invalid_argument"
msgid "Invalid argument for $1: < -1 or > 1"
msgstr ""

#:
msgctxt "pfunc_expr_invalid_argument_ln"
msgid "Invalid argument for ln: <= 0"
msgstr ""

#:
msgctxt "pfunc_expr_unknown_error"
msgid "Expression error: Unknown error ($1)"
msgstr ""

#:
msgctxt "pfunc_expr_not_a_number"
msgid "In $1: result is not a number"
msgstr ""

#. PLURAL is supported for $1.
msgctxt "pfunc_string_too_long"
msgid "Error: String exceeds $1 character limit"
msgstr ""

#. Short description of the Openid extension, shown in [[Special:Version]]. Do not translate or change links.
msgctxt "openid-desc"
msgid "Log in to the wiki with an [http://openid.net/ OpenID], and log in to other OpenID-aware web sites with a wiki user account"
msgstr ""

#:
msgctxt "openidlogin"
msgid "Log in with OpenID"
msgstr ""

#:
msgctxt "openidserver"
msgid "OpenID server"
msgstr ""

#:
msgctxt "openidxrds"
msgid "Yadis file"
msgstr ""

#:
msgctxt "openidconvert"
msgid "OpenID converter"
msgstr ""

#:
msgctxt "openiderror"
msgid "Verification error"
msgstr ""

#:
msgctxt "openiderrortext"
msgid "An error occured during verification of the OpenID URL."
msgstr ""

#:
msgctxt "openidconfigerror"
msgid "OpenID configuration error"
msgstr ""

#:
msgctxt "openidconfigerrortext"
msgid "The OpenID storage configuration for this wiki is invalid.\nPlease consult an [[Special:ListUsers/sysop|administrator]]."
msgstr ""

#:
msgctxt "openidpermission"
msgid "OpenID permissions error"
msgstr ""

#:
msgctxt "openidpermissiontext"
msgid "The OpenID you provided is not allowed to login to this server."
msgstr ""

#:
msgctxt "openidcancel"
msgid "Verification cancelled"
msgstr ""

#:
msgctxt "openidcanceltext"
msgid "Verification of the OpenID URL was cancelled."
msgstr ""

#:
msgctxt "openidfailure"
msgid "Verification failed"
msgstr ""

#:
msgctxt "openidfailuretext"
msgid "Verification of the OpenID URL failed. Error message: \"$1\""
msgstr ""

#:
msgctxt "openidsuccess"
msgid "Verification succeeded"
msgstr ""

#:
msgctxt "openidsuccesstext"
msgid "Verification of the OpenID URL succeeded."
msgstr ""

#:
msgctxt "openidusernameprefix"
msgid "OpenIDUser"
msgstr ""

#:
msgctxt "openidserverlogininstructions"
msgid "Enter your password below to log in to $3 as user $2 (user page $1)."
msgstr ""

#. * $1 is a trust root. A trust root looks much like a normal URL, but is used to describe a set of URLs. Trust roots are used by OpenID to verify that a user has approved the OpenID enabled website.
msgctxt "openidtrustinstructions"
msgid "Check if you want to share data with $1."
msgstr ""

#:
msgctxt "openidallowtrust"
msgid "Allow $1 to trust this user account."
msgstr ""

#:
msgctxt "openidnopolicy"
msgid "Site has not specified a privacy policy."
msgstr ""

#:
msgctxt "openidpolicy"
msgid "Check the <a target=\"_new\" href=\"$1\">privacy policy</a> for more information."
msgstr ""

#. {{Identical|Optional}}
msgctxt "openidoptional"
msgid "Optional"
msgstr ""

#:
msgctxt "openidrequired"
msgid "Required"
msgstr ""

#:
msgctxt "openidnickname"
msgid "Nickname"
msgstr ""

#:
msgctxt "openidfullname"
msgid "Fullname"
msgstr ""

#. {{Identical|E-mail address}}
msgctxt "openidemail"
msgid "E-mail address"
msgstr ""

#. {{Identical|Language}}
msgctxt "openidlanguage"
msgid "Language"
msgstr ""

#. {{Identical|Time zone}}
msgctxt "openidtimezone"
msgid "Time zone"
msgstr ""

#:
msgctxt "openidchooselegend"
msgid "Username choice"
msgstr ""

#:
msgctxt "openidchooseinstructions"
msgid "All users need a nickname;\nyou can choose one from the options below."
msgstr ""

#:
msgctxt "openidchoosenick"
msgid "Your nickname ($1)"
msgstr ""

#:
msgctxt "openidchoosefull"
msgid "Your full name ($1)"
msgstr ""

#:
msgctxt "openidchooseurl"
msgid "A name picked from your OpenID ($1)"
msgstr ""

#:
msgctxt "openidchooseauto"
msgid "An auto-generated name ($1)"
msgstr ""

#:
msgctxt "openidchoosemanual"
msgid "A name of your choice:"
msgstr ""

#:
msgctxt "openidchooseexisting"
msgid "An existing account on this wiki"
msgstr ""

#:
msgctxt "openidchooseusername"
msgid "Username:"
msgstr ""

#. {{Identical|Password}}
msgctxt "openidchoosepassword"
msgid "Password:"
msgstr ""

#:
msgctxt "openidconvertinstructions"
msgid "This form lets you change your user account to use an OpenID URL or add more OpenID URLs"
msgstr ""

#:
msgctxt "openidconvertoraddmoreids"
msgid "Convert to OpenID or add another OpenID URL"
msgstr ""

#:
msgctxt "openidconvertsuccess"
msgid "Successfully converted to OpenID"
msgstr ""

#:
msgctxt "openidconvertsuccesstext"
msgid "You have successfully converted your OpenID to $1."
msgstr ""

#:
msgctxt "openidconvertyourstext"
msgid "That is already your OpenID."
msgstr ""

#:
msgctxt "openidconvertothertext"
msgid "That is someone else's OpenID."
msgstr ""

#. $1 is a user name.
msgctxt "openidalreadyloggedin"
msgid "'''You are already logged in, $1!'''\n\nIf you want to use OpenID to log in in the future, you can [[Special:OpenIDConvert|convert your account to use OpenID]]."
msgstr ""

#:
msgctxt "openidnousername"
msgid "No username specified."
msgstr ""

#:
msgctxt "openidbadusername"
msgid "Bad username specified."
msgstr ""

#. {{doc-important|"Continue" will never be localised. It is hardcoded in a PHP extension. Translations could be made like ""Continue" (translation)"}}
msgctxt "openidautosubmit"
msgid "This page includes a form that should be automatically submitted if you have JavaScript enabled.\nIf not, try the \"Continue\" button."
msgstr ""

#:
msgctxt "openidclientonlytext"
msgid "You cannot use accounts from this wiki as OpenIDs on another site."
msgstr ""

#:
msgctxt "openidloginlabel"
msgid "OpenID URL"
msgstr ""

#:
msgctxt "openidlogininstructions"
msgid "{{SITENAME}} supports the [http://openid.net/ OpenID] standard for single sign-on between websites.\nOpenID lets you log in to many different websites without using a different password for each.\n(See [http://en.wikipedia.org/wiki/OpenID Wikipedia's OpenID article] for more information.)\n\nIf you already have an account on {{SITENAME}}, you can [[Special:UserLogin|log in]] with your username and password as usual.\nTo use OpenID in the future, you can [[Special:OpenIDConvert|convert your account to OpenID]] after you have logged in normally.\n\nThere are many [http://openid.net/get/ OpenID providers], and you may already have an OpenID-enabled account on another service."
msgstr ""

#:
msgctxt "openidupdateuserinfo"
msgid "Update my personal information:"
msgstr ""

#:
msgctxt "openiddelete"
msgid "Delete OpenID"
msgstr ""

#:
msgctxt "openiddelete-text"
msgid "By clicking the \"{{int:openiddelete-button}}\" button, you will remove the OpenID $1 from your account.\nYou will no longer be able to log in with this OpenID."
msgstr ""

#. {{Identical|Confirm}}
msgctxt "openiddelete-button"
msgid "Confirm"
msgstr ""

#:
msgctxt "openiddeleteerrornopassword"
msgid "You cannot delete all your OpenIDs because your account has no password.\nYou would not able to log in without an OpenID."
msgstr ""

#:
msgctxt "openiddeleteerroropenidonly"
msgid "You cannot delete all your OpenIDs because your are only allowed to log in with OpenID.\nYou would not able to log in without an OpenID."
msgstr ""

#:
msgctxt "openiddelete-sucess"
msgid "The OpenID has been successfully removed from your account."
msgstr ""

#:
msgctxt "openiddelete-error"
msgid "An error occured while removing the OpenID from your account."
msgstr ""

#. OpenID preferences tab text above the list of preferences
msgctxt "openid-prefstext"
msgid "[http://openid.net/ OpenID] preferences"
msgstr ""

#. OpenID preference label (Hide your OpenID URL on your user page, if you log in with OpenID)
msgctxt "openid-pref-hide"
msgid "Hide your OpenID URL on your user page, if you log in with OpenID."
msgstr ""

#. OpenID preference label for updating fron OpenID persona upon login
msgctxt "openid-pref-update-userinfo-on-login"
msgid "Update the following information from OpenID persona every time I log in:"
msgstr ""

#:
msgctxt "openid-urls-desc"
msgid "OpenIDs associated with your account:"
msgstr ""

#. {{Identical|Action}}
msgctxt "openid-urls-action"
msgid "Action"
msgstr ""

#. {{identical|Delete}}
msgctxt "openid-urls-delete"
msgid "Delete"
msgstr ""

#:
msgctxt "openid-add-url"
msgid "Add a new OpenID"
msgstr ""

#:
msgctxt "openidsigninorcreateaccount"
msgid "Sign in or create new account"
msgstr ""

#:
msgctxt "openid-provider-label-openid"
msgid "Enter your OpenID URL"
msgstr ""

#:
msgctxt "openid-provider-label-google"
msgid "Log in using your Google account"
msgstr ""

#:
msgctxt "openid-provider-label-yahoo"
msgid "Log in using your Yahoo account"
msgstr ""

#:
msgctxt "openid-provider-label-aol"
msgid "Enter your AOL screenname"
msgstr ""

#:
msgctxt "openid-provider-label-other-username"
msgid "Enter your $1 username"
msgstr ""

#. Short description of this extension, shown on [[Special:Version]]. Do not translate or change links.
msgctxt "microid-desc"
msgid "Adds a [http://www.microid.org/ MicroID] to user pages to confirm account with external services"
msgstr ""

#:
msgctxt "tog-microid"
msgid "Publish a <a href=\"http://microid.org/\">MicroID</a> to confirm account with external services"
msgstr ""

#. Short description of the Inputbox extension, shown on [[Special:Version]].
msgctxt "inputbox-desc"
msgid "Allow inclusion of predefined HTML forms"
msgstr ""

#:
msgctxt "inputbox-error-no-type"
msgid "You have not specified the type of input box to create."
msgstr ""

#. {{doc-important|"create", "comment", "search", "search2" and "fulltext" should not be translated.}}
msgctxt "inputbox-error-bad-type"
msgid "Input box type \"$1\" not recognised.\nPlease specify \"create\", \"comment\", \"search\", \"search2\" or \"fulltext\"."
msgstr ""

#. Part of the "Inputbox" extension. This message is the text of the button to search the page you typed in the inputbox. If the page with the exact name exists, you will go directly to that page.
msgctxt "tryexact"
msgid "Try exact match"
msgstr "Kërko përputhje të plotë"

#. Part of the "Inputbox" extension. This message is the text of the button to search the page you typed in the inputbox. This button always goes to the search page, even if the page with the exact name exists.
msgctxt "searchfulltext"
msgid "Search full text"
msgstr "Kërko tekstin e plotë"

#. Part of the "Inputbox" extension. This message is the text of the button to create the page you typed in the inputbox.
msgctxt "createarticle"
msgid "Create page"
msgstr "Krijo artikull"

#. Short description of the CharInsert extension, shown on [[Special:Version]]. Do not translate or change links.
msgctxt "charinsert-desc"
msgid "Allows creation of JavaScript box for [[MediaWiki:Edittools|inserting non-standard characters]]"
msgstr "Lejon krijimin e kutive JavaScript për [[MediaWiki:Edittools|inserting karaktere jo standarde]]"

