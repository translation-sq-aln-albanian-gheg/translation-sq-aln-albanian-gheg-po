# Translation of User Contact Links to Gheg Albanian
msgid ""
msgstr ""
"Project-Id-Version: MediaWiki 1.17alpha (r66640)\n"
"Report-Msgid-Bugs-To: http://translatewiki.net\n"
"POT-Creation-Date: 2010-05-19 17:42:51+0000\n"
"Language-Team: Gheg Albanian\n"
"Content-Type: text-plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: MediaWiki Translate extension 2010-05-15\n"
"X-Language-Code: aln\n"
"X-Message-Group: ext-usercontactlinks\n"

#. {{Identical|Incorrect username}}
msgctxt "usercontactlink-baduser"
msgid "incorrect username"
msgstr "Emri i gabuar"

#. {{desc}}
msgctxt "usercontactlinks-desc"
msgid "Provides the ability to simply and consistently add other user names using ^^^user^^^ syntax"
msgstr "Ofron mundësinë për të thjesht dhe vazhdimisht shtoni emra të tjerë përdorues duke përdorur ^ ^ ^ Përdorues ^ ^ ^ sintakse"

