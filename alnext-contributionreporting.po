# Translation of Contribution Reporting to Gheg Albanian
msgid ""
msgstr ""
"Project-Id-Version: MediaWiki 1.17alpha (r66640)\n"
"Report-Msgid-Bugs-To: http://translatewiki.net\n"
"POT-Creation-Date: 2010-05-19 17:12:02+0000\n"
"Language-Team: Gheg Albanian\n"
"Content-Type: text-plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: MediaWiki Translate extension 2010-05-15\n"
"X-Language-Code: aln\n"
"X-Message-Group: ext-contributionreporting\n"

#. Description for the ContributionReporting Extension. Shown in [[Special:Version]]. Do not translate or change tag names.
msgctxt "contributionreporting-desc"
msgid "Live reporting on the Wikimedia fundraiser"
msgstr ""

#:
msgctxt "contributiontotal"
msgid "Contribution total"
msgstr ""

#:
msgctxt "contributionhistory"
msgid "Contribution history"
msgstr ""

#. Title of Special:ContributionHistory. See http://wikimediafoundation.org/wiki/Special:ContributionHistory for example.
msgctxt "contrib-hist-header"
msgid "Real-time donor comments from around the world"
msgstr ""

#. {{Identical|Name}}
msgctxt "contrib-hist-name"
msgid "Name"
msgstr ""

#. {{Identical|Date}}
msgctxt "contrib-hist-date"
msgid "Time and date"
msgstr ""

#. {{Identical|Amount}}
msgctxt "contrib-hist-amount"
msgid "Amount"
msgstr ""

#:
msgctxt "contrib-hist-next"
msgid "Earlier donations"
msgstr ""

#:
msgctxt "contrib-hist-previous"
msgid "Newer donations"
msgstr ""

#:
msgctxt "contrib-hist-anonymous"
msgid "Anonymous"
msgstr ""

#:
msgctxt "contributionstatistics"
msgid "Contribution statistics"
msgstr ""

#:
msgctxt "contribstats-desc"
msgid "Displays statistics for contributions made to the Wikimedia Foundation"
msgstr ""

#:
msgctxt "contribstats-daily-totals"
msgid "{{PLURAL:$1|Total for today|Daily totals for the past $1 days}}"
msgstr ""

#:
msgctxt "contribstats-monthly-totals"
msgid "{{PLURAL:$1|Total for this month|Monthly totals for the past $1 months}}"
msgstr ""

#:
msgctxt "contribstats-month-range-totals"
msgid "{{PLURAL:$1|Total for $1 month|Monthly totals for $1 months}}"
msgstr ""

#. * $1 is a start date
#. * $2 is an end date
msgctxt "contribstats-currency-range-totals"
msgid "Currency totals (from $1 to $2)"
msgstr ""

#:
msgctxt "contribstats-contribution-range-breakdown"
msgid "Breakdown of contributions by value (from $1 to $2)"
msgstr ""

#:
msgctxt "contribstats-currency-totals"
msgid "Currency totals for the fiscal year of $1"
msgstr ""

#:
msgctxt "contribstats-contribution-breakdown"
msgid "Breakdown of contributions by value for the fiscal year of $1"
msgstr ""

#:
msgctxt "contribstats-day"
msgid "Day"
msgstr ""

#:
msgctxt "contribstats-month"
msgid "Month"
msgstr ""

#. {{Identical|Currency}}
msgctxt "contribstats-currency"
msgid "Currency"
msgstr ""

#:
msgctxt "contribstats-amount"
msgid "Amount (USD)"
msgstr ""

#:
msgctxt "contribstats-contributions"
msgid "Contributions"
msgstr ""

#:
msgctxt "contribstats-total"
msgid "Total (USD)"
msgstr ""

#:
msgctxt "contribstats-avg"
msgid "Average (USD)"
msgstr ""

#:
msgctxt "contribstats-max"
msgid "Highest Donation"
msgstr ""

#:
msgctxt "contribstats-percentage-ytd"
msgid "Percentage (YTD)"
msgstr ""

#:
msgctxt "contribstats-total-ytd"
msgid "Total (YTD)"
msgstr ""

#:
msgctxt "contribstats-value-exactly"
msgid "Exactly $1"
msgstr ""

#:
msgctxt "contribstats-value-under"
msgid "Under $1"
msgstr ""

#:
msgctxt "contribstats-value-from"
msgid "From $1 - $2"
msgstr ""

#:
msgctxt "contribstats-value-over"
msgid "Over $1"
msgstr ""

#:
msgctxt "contributiontrackingstatistics"
msgid "Contribution tracking statistics"
msgstr ""

#. The day for which the contribution statistics are being displayed.
msgctxt "contribstats-day-totals"
msgid "Total for day"
msgstr ""

#:
msgctxt "contribstats-week"
msgid "Week"
msgstr ""

#:
msgctxt "contribstats-weekly-totals"
msgid "{{PLURAL:$1|Total for this week|Weekly totals for the past $1 weeks inclusive}}"
msgstr ""

#:
msgctxt "contribstats-payment-type"
msgid "Payment type"
msgstr ""

#:
msgctxt "contribstats-banner"
msgid "Banner"
msgstr ""

#:
msgctxt "contribstats-payment-type-hits"
msgid "Payment type hits"
msgstr ""

#:
msgctxt "contribstats-clicks"
msgid "Clicks"
msgstr ""

#:
msgctxt "contribstats-donations"
msgid "Donations"
msgstr ""

#. Click conversion rate, see [[:wikipedia:Conversion rate|Wikipedia article]].
msgctxt "contribstats-conversion"
msgid "Conversion rate (%)"
msgstr ""

#. {{Identical|Template}}
msgctxt "contribstats-template"
msgid "Template"
msgstr ""

#:
msgctxt "contribstats-nodata"
msgid "No data provided"
msgstr ""

#:
msgctxt "contribstats-landingpage"
msgid "Landing page"
msgstr ""

#:
msgctxt "contribstats-donatepage"
msgid "Donate page"
msgstr ""

#. {{Identical|Average}}
msgctxt "contribstats-average"
msgid "Average"
msgstr ""

#:
msgctxt "contribstats-imperfect-data"
msgid "This data is not perfect as tracking donors without using session tracking has its limitations."
msgstr ""

#:
msgctxt "contribstats-paypal-donations"
msgid "PayPal donations"
msgstr ""

#:
msgctxt "contribstats-credit-card"
msgid "Credit card"
msgstr ""

#:
msgctxt "contribstats-fraud-note"
msgid "Some fraudulent donations exist."
msgstr ""

#:
msgctxt "contribstats-unaudited"
msgid "These are unaudited totals."
msgstr ""

#:
msgctxt "fundraiserstatistics"
msgid "Fundraiser statistics"
msgstr ""

#. {{Identical|Date}}
msgctxt "fundraiserstats-date"
msgid "Date"
msgstr ""

#. * $1 is the nth day of an annual fundraiser
#. * $2 is the year/name of a fundraiser
msgctxt "fundraiserstats-day"
msgid "Day $1 of $2"
msgstr ""

#:
msgctxt "fundraiserstats-contributions"
msgid "Contributions"
msgstr ""

#:
msgctxt "fundraiserstats-total"
msgid "Total (USD)"
msgstr ""

#:
msgctxt "fundraiserstats-avg"
msgid "Average (USD)"
msgstr ""

#:
msgctxt "fundraiserstats-ytd"
msgid "Cumulative total (USD)"
msgstr ""

#:
msgctxt "fundraiserstats-max"
msgid "Maximum (USD)"
msgstr ""

#:
msgctxt "fundraiserstats-tab-totals"
msgid "Totals (USD)"
msgstr ""

#:
msgctxt "fundraiserstats-tab-contributions"
msgid "Number of contributions"
msgstr ""

#:
msgctxt "fundraiserstats-tab-averages"
msgid "Averages (USD)"
msgstr ""

#:
msgctxt "fundraiserstats-tab-maximums"
msgid "Maximums (USD)"
msgstr ""

#:
msgctxt "fundraiserstats-tab-ytd"
msgid "Year-to-date (USD)"
msgstr ""

