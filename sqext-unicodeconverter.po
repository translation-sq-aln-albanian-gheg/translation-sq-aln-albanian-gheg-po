# Translation of Unicode Converter to Albanian
msgid ""
msgstr ""
"Project-Id-Version: MediaWiki 1.17alpha (r66643)\n"
"Report-Msgid-Bugs-To: http://translatewiki.net\n"
"POT-Creation-Date: 2010-05-19 18:32:48+0000\n"
"Language-Team: Albanian\n"
"Content-Type: text-plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: MediaWiki Translate extension 2010-05-15\n"
"X-Language-Code: sq\n"
"X-Message-Group: ext-unicodeconverter\n"

#:
msgctxt "unicodeconverter"
msgid "Unicode converter"
msgstr ""

#. Shown in the special page [[Special:Version]] as a short description of the extension.
msgctxt "unicodeconverter-desc"
msgid "A simple example of a special page module. [[Special:UnicodeConverter|Given a string in UTF-8]], it converts it to HTML entities suitable for an ISO 8859-1 web page"
msgstr ""

#. {{Identical|OK}}
msgctxt "unicodeconverter-ok"
msgid "OK"
msgstr ""

#:
msgctxt "unicodeconverter-oldtext"
msgid "Original text:"
msgstr ""

#:
msgctxt "unicodeconverter-newtext"
msgid "Converted text:"
msgstr ""

