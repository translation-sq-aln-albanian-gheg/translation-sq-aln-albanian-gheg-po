# Translation of Invitations to Gheg Albanian
msgid ""
msgstr ""
"Project-Id-Version: MediaWiki 1.17alpha (r66640)\n"
"Report-Msgid-Bugs-To: http://translatewiki.net\n"
"POT-Creation-Date: 2010-05-19 17:21:34+0000\n"
"Language-Team: Gheg Albanian\n"
"Content-Type: text-plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: MediaWiki Translate extension 2010-05-15\n"
"X-Language-Code: aln\n"
"X-Message-Group: ext-invitations\n"

#:
msgctxt "invite-logpage"
msgid "Invitation log"
msgstr ""

#:
msgctxt "invite-logpagetext"
msgid "This is a log of users inviting each other to use various software features."
msgstr ""

#:
msgctxt "invite-logentry"
msgid "invited $1 to use the <i>$2</i> feature."
msgstr ""

#:
msgctxt "invitations"
msgid "Manage invitations to software features"
msgstr ""

#. Short description of this extension, shown on [[Special:Version]]. Do not translate or change links.
msgctxt "invitations-desc"
msgid "Allows [[Special:Invitations|management of new features]] by restricting them to an invitation-based system"
msgstr ""

#:
msgctxt "invitations-invitedlist-description"
msgid "You have access to the following invitation-only software features.\nTo manage invitations for an individual feature, click on its name."
msgstr ""

#:
msgctxt "invitations-invitedlist-none"
msgid "You have not been invited to use any invitation-only software features."
msgstr ""

#:
msgctxt "invitations-invitedlist-item-count"
msgid "({{PLURAL:$1|One invitation|$1 invitations}} available)"
msgstr ""

#:
msgctxt "invitations-pagetitle"
msgid "Invite-only software features"
msgstr ""

#:
msgctxt "invitations-uninvitedlist-description"
msgid "You do not have access to these other invitation-only software features."
msgstr ""

#:
msgctxt "invitations-uninvitedlist-none"
msgid "At this time, no other software features are designated invitation-only."
msgstr ""

#:
msgctxt "invitations-feature-pagetitle"
msgid "Invitation management - $1"
msgstr ""

#:
msgctxt "invitations-feature-access"
msgid "You currently have access to use <i>$1</i>."
msgstr ""

#:
msgctxt "invitations-feature-numleft"
msgid "You still have {{PLURAL:$2|one invitation left|<b>$1</b> out of your $2 invitations left}}."
msgstr ""

#:
msgctxt "invitations-feature-noneleft"
msgid "You have used all of your allocated invitations for this feature"
msgstr ""

#:
msgctxt "invitations-feature-noneyet"
msgid "You have not yet received your allocation of invitations for this feature."
msgstr ""

#:
msgctxt "invitations-feature-notallowed"
msgid "You do not have access to use <i>$1</i>."
msgstr ""

#:
msgctxt "invitations-inviteform-title"
msgid "Invite a user to use $1"
msgstr ""

#:
msgctxt "invitations-inviteform-username"
msgid "User to invite"
msgstr ""

#:
msgctxt "invitations-inviteform-submit"
msgid "Invite"
msgstr ""

#:
msgctxt "invitations-error-baduser"
msgid "The user you specified does not appear to exist."
msgstr ""

#:
msgctxt "invitations-error-alreadyinvited"
msgid "The user you specified already has access to this feature!"
msgstr ""

#:
msgctxt "invitations-invite-success"
msgid "You have successfully invited $1 to use this feature!"
msgstr ""

