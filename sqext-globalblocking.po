# Translation of Global Blocking to Albanian
msgid ""
msgstr ""
"Project-Id-Version: MediaWiki 1.17alpha (r66640)\n"
"Report-Msgid-Bugs-To: http://translatewiki.net\n"
"POT-Creation-Date: 2010-05-19 18:09:35+0000\n"
"Language-Team: Albanian\n"
"Content-Type: text-plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: MediaWiki Translate extension 2010-05-15\n"
"X-Language-Code: sq\n"
"X-Message-Group: ext-globalblocking\n"

#. Short description of this extension, shown on [[Special:Version]]. Do not translate or change links.
msgctxt "globalblocking-desc"
msgid "[[Special:GlobalBlock|Allows]] IP addresses to be [[Special:GlobalBlockList|blocked across multiple wikis]]"
msgstr ""

#. Same special page with this page:
#. 
#. * [[MediaWiki:Globalblock/{{SUBPAGENAME}}]]
msgctxt "globalblocking-block"
msgid "Globally block an IP address"
msgstr ""

#:
msgctxt "globalblocking-modify-intro"
msgid "You can use this form to change the settings of a global block."
msgstr ""

#:
msgctxt "globalblocking-block-intro"
msgid "You can use this page to block an IP address on all wikis."
msgstr ""

#. {{Identical/Reason}}
msgctxt "globalblocking-block-reason"
msgid "Reason:"
msgstr ""

#. {{Identical/Other/additional reason}}
msgctxt "globalblocking-block-otherreason"
msgid "Other/additional reason:"
msgstr ""

#. {{Identical|Other reason}}
msgctxt "globalblocking-block-reasonotherlist"
msgid "Other reason"
msgstr ""

#:
msgctxt "globalblocking-block-reason-dropdown"
msgid "* Common block reasons\n** Crosswiki spamming\n** Crosswiki abuse\n** Vandalism"
msgstr ""

#:
msgctxt "globalblocking-block-edit-dropdown"
msgid "Edit block reasons"
msgstr ""

#. {{Identical/Expiry}}
msgctxt "globalblocking-block-expiry"
msgid "Expiry:"
msgstr ""

#:
msgctxt "globalblocking-block-expiry-other"
msgid "Other expiry time"
msgstr ""

#. {{Identical|Other time}}
msgctxt "globalblocking-block-expiry-otherfield"
msgid "Other time:"
msgstr ""

#:
msgctxt "globalblocking-block-legend"
msgid "Block an IP address globally"
msgstr ""

#. {{Identical|Options}}
msgctxt "globalblocking-block-options"
msgid "Options:"
msgstr ""

#. The first line of the error message shown on [[Special:GlobalBlock]] (see [[mw:Extension:GlobalBlocking]]) if the block has been unsuccessful. After this message, a list of specific errors is shown (see [[Special:Prefixindex/MediaWiki:Globalblocking-block-bigrange|globalblocking-block-bigrange]], [[Special:Prefixindex/MediaWiki:Globalblocking-block-expiryinvalid|globalblocking-block-expiryinvalid]] etc.).
#. 
#. * $1 – the ''number'' of errors (not the errors themselves)
msgctxt "globalblocking-block-errors"
msgid "Your block was unsuccessful, for the following {{PLURAL:$1|reason|reasons}}:"
msgstr ""

#. {{Identical|The IP address ($1) ...}}
msgctxt "globalblocking-block-ipinvalid"
msgid "The IP address ($1) you entered is invalid.\nPlease note that you cannot enter a user name!"
msgstr ""

#:
msgctxt "globalblocking-block-expiryinvalid"
msgid "The expiry you entered ($1) is invalid."
msgstr ""

#:
msgctxt "globalblocking-block-submit"
msgid "Block this IP address globally"
msgstr ""

#:
msgctxt "globalblocking-modify-submit"
msgid "Modify this global block"
msgstr ""

#:
msgctxt "globalblocking-block-success"
msgid "The IP address $1 has been successfully blocked on all projects."
msgstr ""

#:
msgctxt "globalblocking-modify-success"
msgid "The global block on $1 has been successfully modified"
msgstr ""

#:
msgctxt "globalblocking-block-successsub"
msgid "Global block successful"
msgstr ""

#:
msgctxt "globalblocking-modify-successsub"
msgid "Global block modified successfully"
msgstr ""

#:
msgctxt "globalblocking-block-alreadyblocked"
msgid "The IP address $1 is already blocked globally.\nYou can view the existing block on the [[Special:GlobalBlockList|list of global blocks]],\nor modify the settings of the existing block by re-submitting this form."
msgstr ""

#. Not clear at all what the English message means about ranges. Unfortunately, http://www.mediawiki.org/wiki/Extension:GlobalBlocking supplies no explanation whatsoever.
msgctxt "globalblocking-block-bigrange"
msgid "The range you specified ($1) is too big to block.\nYou may block, at most, 65,536 addresses (/16 ranges)"
msgstr ""

#. Appears on top of [[Special:Globalblocklist]] (part of [[mw:Extension:GlobalBlocking|Extension:GlobalBlocking]], which is not installed on translatewiki.net; example: [[wikipedia:Special:Globalblocklist]]).
msgctxt "globalblocking-list-intro"
msgid "This is a list of all global blocks which are currently in effect.\nSome blocks are marked as locally disabled: this means that they apply on other sites, but a local administrator has decided to disable them on this wiki."
msgstr ""

#:
msgctxt "globalblocking-list"
msgid "List of globally blocked IP addresses"
msgstr ""

#:
msgctxt "globalblocking-search-legend"
msgid "Search for a global block"
msgstr ""

#. {{Identical|IP Address}}
msgctxt "globalblocking-search-ip"
msgid "IP address:"
msgstr ""

#:
msgctxt "globalblocking-search-submit"
msgid "Search for blocks"
msgstr ""

#:
msgctxt "globalblocking-list-ipinvalid"
msgid "The IP address you searched for ($1) is invalid.\nPlease enter a valid IP address."
msgstr ""

#:
msgctxt "globalblocking-search-errors"
msgid "Your search was unsuccessful, for the following {{PLURAL:$1|reason|reasons}}:"
msgstr ""

#. * $1 is a time stamp
#. * $2 is the blocking user
#. * $3 is the source wiki for the blocking user
#. * $4 is the blocked user
#. * $5 are the block options
msgctxt "globalblocking-list-blockitem"
msgid "$1: <span class=\"plainlinks\">'''$2'''</span> (''$3'') globally blocked [[Special:Contributions/$4|$4]] ''($5)''"
msgstr ""

#:
msgctxt "globalblocking-list-expiry"
msgid "expiry $1"
msgstr ""

#. {{Identical|Anon only}}
msgctxt "globalblocking-list-anononly"
msgid "anonymous only"
msgstr ""

#:
msgctxt "globalblocking-list-unblock"
msgid "remove"
msgstr ""

#:
msgctxt "globalblocking-list-whitelisted"
msgid "locally disabled by $1: $2"
msgstr ""

#. {{Identical|Local status}}
msgctxt "globalblocking-list-whitelist"
msgid "local status"
msgstr ""

#:
msgctxt "globalblocking-list-modify"
msgid "modify"
msgstr ""

#:
msgctxt "globalblocking-list-noresults"
msgid "The requested IP address is not blocked."
msgstr ""

#:
msgctxt "globalblocking-goto-block"
msgid "Globally block an IP address"
msgstr ""

#:
msgctxt "globalblocking-goto-unblock"
msgid "Remove a global block"
msgstr ""

#:
msgctxt "globalblocking-goto-status"
msgid "Change local status for a global block"
msgstr ""

#:
msgctxt "globalblocking-return"
msgid "Return to the list of global blocks"
msgstr ""

#:
msgctxt "globalblocking-notblocked"
msgid "The IP address ($1) you entered is not globally blocked."
msgstr ""

#:
msgctxt "globalblocking-unblock"
msgid "Remove a global block"
msgstr ""

#. {{Identical|The IP address ($1) ...}}
msgctxt "globalblocking-unblock-ipinvalid"
msgid "The IP address ($1) you entered is invalid.\nPlease note that you cannot enter a user name!"
msgstr ""

#:
msgctxt "globalblocking-unblock-legend"
msgid "Remove a global block"
msgstr ""

#:
msgctxt "globalblocking-unblock-submit"
msgid "Remove global block"
msgstr ""

#. {{Identical|Reason}}
msgctxt "globalblocking-unblock-reason"
msgid "Reason:"
msgstr ""

#:
msgctxt "globalblocking-unblock-unblocked"
msgid "You have successfully removed the global block #$2 on the IP address '''$1'''"
msgstr ""

#:
msgctxt "globalblocking-unblock-errors"
msgid "Your removal of the global block was unsuccessful, for the following {{PLURAL:$1|reason|reasons}}:"
msgstr ""

#:
msgctxt "globalblocking-unblock-successsub"
msgid "Global block successfully removed"
msgstr ""

#:
msgctxt "globalblocking-unblock-subtitle"
msgid "Removing global block"
msgstr ""

#:
msgctxt "globalblocking-unblock-intro"
msgid "You can use this form to remove a global block."
msgstr ""

#:
msgctxt "globalblocking-whitelist"
msgid "Local status of global blocks"
msgstr ""

#:
msgctxt "globalblocking-whitelist-notapplied"
msgid "Global blocks are not applied at this wiki,\nso the local status of global blocks cannot be modified."
msgstr ""

#. {{Identical|Change local status}}
msgctxt "globalblocking-whitelist-legend"
msgid "Change local status"
msgstr ""

#. {{Identical|Reason}}
msgctxt "globalblocking-whitelist-reason"
msgid "Reason:"
msgstr ""

#. {{Identical|Local status}}
msgctxt "globalblocking-whitelist-status"
msgid "Local status:"
msgstr ""

#:
msgctxt "globalblocking-whitelist-statuslabel"
msgid "Disable this global block on {{SITENAME}}"
msgstr ""

#. {{Identical|Change local status}}
msgctxt "globalblocking-whitelist-submit"
msgid "Change local status"
msgstr ""

#:
msgctxt "globalblocking-whitelist-whitelisted"
msgid "You have successfully disabled the global block #$2 on the IP address '''$1''' on {{SITENAME}}."
msgstr ""

#:
msgctxt "globalblocking-whitelist-dewhitelisted"
msgid "You have successfully re-enabled the global block #$2 on the IP address '''$1''' on {{SITENAME}}."
msgstr ""

#:
msgctxt "globalblocking-whitelist-successsub"
msgid "Local status successfully changed"
msgstr ""

#:
msgctxt "globalblocking-whitelist-nochange"
msgid "You made no change to the local status of this block.\n[[Special:GlobalBlockList|Return to the global block list]]."
msgstr ""

#:
msgctxt "globalblocking-whitelist-errors"
msgid "Your change to the local status of a global block was unsuccessful, for the following {{PLURAL:$1|reason|reasons}}:"
msgstr ""

#:
msgctxt "globalblocking-whitelist-intro"
msgid "You can use this form to edit the local status of a global block.\nIf a global block is disabled on this wiki, users on the affected IP address will be able to edit normally.\n[[Special:GlobalBlockList|Return to the global block list]]."
msgstr ""

#. A message shown to a [[mw:Extension:GlobalBlocking|globally blocked]] user trying to edit.
#. 
#. * <code>$1</code> is the username of the blocking user (steward), with link
#. * <code>$2</code> is the project name where the user is registered (usually “meta.wikimedia.org” on Wikimedia servers)
#. * <code>$3</code> is the reason specified by the blocking user
#. * <code>$4</code> is either the contents of {{msg-mw|Infiniteblock}} (''{{int:Infiniteblock}}''), or {{msg-mw|Expiringblock}} (''{{int:Expiringblock}}'') with the expiry time
msgctxt "globalblocking-blocked"
msgid "Your IP address has been blocked on all wikis by '''$1''' (''$2'').\nThe reason given was ''\"$3\"''.\nThe block ''$4''."
msgstr ""

#:
msgctxt "globalblocking-blocked-nopassreset"
msgid "You cannot reset user's passwords because you are blocked globally."
msgstr ""

#:
msgctxt "globalblocking-logpage"
msgid "Global block log"
msgstr ""

#. Shown as header of [[Special:Log/gblblock]] (part of [[mw:Extension:GlobalBlocking|Extension:GlobalBlocking]], which is not installed on translatewiki.net; example: [[wikipedia:Special:Log/gblblock]])
msgctxt "globalblocking-logpagetext"
msgid "This is a log of global blocks which have been made and removed on this wiki.\nIt should be noted that global blocks can be made and removed on other wikis, and that these global blocks may affect this wiki.\nTo view all active global blocks, you may view the [[Special:GlobalBlockList|global block list]]."
msgstr ""

#:
msgctxt "globalblocking-block-logentry"
msgid "globally blocked [[$1]] with an expiry time of $2"
msgstr ""

#. * $1 is a link to a user page of the form User:Name
#. * $2 is a reason for the action.
msgctxt "globalblocking-block2-logentry"
msgid "globally blocked [[$1]] ($2)"
msgstr ""

#. This message is a log entry. '''$1''' are contributions of an IP. For an example see http://meta.wikimedia.org/wiki/Special:Log/gblblock?uselang=en
msgctxt "globalblocking-unblock-logentry"
msgid "removed global block on [[$1]]"
msgstr ""

#:
msgctxt "globalblocking-whitelist-logentry"
msgid "disabled the global block on [[$1]] locally"
msgstr ""

#:
msgctxt "globalblocking-dewhitelist-logentry"
msgid "re-enabled the global block on [[$1]] locally"
msgstr ""

#. $1 is a link to a user page of the form User:Name, $2 is a reason for the action.
msgctxt "globalblocking-modify-logentry"
msgid "modified the global block on [[$1]] ($2)"
msgstr ""

#:
msgctxt "globalblocking-logentry-expiry"
msgid "expires $1"
msgstr ""

#:
msgctxt "globalblocking-logentry-noexpiry"
msgid "no expiry set"
msgstr ""

#. Shown at Special:IPBlocklist when the GlobalBlocking extension is enabled (not on translatewiki).
#. * $1 is the requested IP address
msgctxt "globalblocking-loglink"
msgid "The IP address $1 is blocked globally ([[{{#Special:GlobalBlockList}}/$1|full details]])."
msgstr ""

#:
msgctxt "globalblocking-showlog"
msgid "This IP address has been blocked previously.\nThe block log is provided below for reference:"
msgstr ""

#:
msgctxt "globalblocklist"
msgid "List of globally blocked IP addresses"
msgstr ""

#. Same special page with this page:
#. 
#. * [[MediaWiki:Globalblocking-block/{{SUBPAGENAME}}]]
msgctxt "globalblock"
msgid "Globally block an IP address"
msgstr ""

#:
msgctxt "globalblockstatus"
msgid "Local status of global blocks"
msgstr ""

#:
msgctxt "removeglobalblock"
msgid "Remove a global block"
msgstr ""

#. {{doc-right}}
msgctxt "right-globalblock"
msgid "Make global blocks"
msgstr ""

#. {{doc-right}}
msgctxt "right-globalunblock"
msgid "Remove global blocks"
msgstr ""

#. {{doc-right}}
msgctxt "right-globalblock-whitelist"
msgid "Disable global blocks locally"
msgstr ""

#. {{doc-right}}
msgctxt "right-globalblock-exempt"
msgid "Bypass global blocks"
msgstr ""

